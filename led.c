// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) Holger Buss, Ingo Busker
// + Nur für den privaten Gebrauch
// + www.MikroKopter.com
// + porting the sources to other systems or using the software on other systems (except hardware from www.mikrokopter.de) is not allowed
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten (nicht-kommerziellen) Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder sonstigen Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <avr/io.h>

#include "eeprom.h"
#include "fc.h"
#include "hwinfo.h"
#include "led.h"

uint8_t J16Blinkcount = 0;
uint8_t J16Mask = 1;

uint8_t J17Blinkcount = 0;
uint8_t J17Mask = 1;

// Output

inline void Output_J16_Set(bool on)
{
	if (on) {
		PORTC |= _BV(PORTC2);
	} else {
		PORTC &= ~_BV(PORTC2);
	}
}

inline void Output_J17_Set(bool on)
{
	if (on) {
		PORTC |= _BV(PORTC3);
	} else {
		PORTC &= ~_BV(PORTC3);
	}
}

// Lifecycle

void LED_Init(void)
{
	// set PC2 & PC3 as output (control of J16 & J17)
	DDRC |= _BV(DDC2) | _BV(DDC3);

	Output_J16_Set(false);
	Output_J17_Set(false);

	J16Blinkcount = 0;
	J16Mask = 128;

	J17Blinkcount = 0;
	J17Mask = 128;
}

void LED_Update(void)
{
	static uint8_t delay = 0;
	static uint8_t J16Bitmask = 0;
	static uint8_t J17Bitmask = 0;

	if(!delay--) // 10 ms intervall
	{
		delay = 4;

		if(MKFlags & MKFLAG_LOWBAT)
		{
			J16Bitmask = ParamSet.J16Bitmask_Warning;
			J17Bitmask = ParamSet.J17Bitmask_Warning;
		}
		else
		{
			J16Bitmask = ParamSet.J16Bitmask;
			J17Bitmask = ParamSet.J17Bitmask;
		}

		if ((ParamSet.Config1 & CFG1_MOTOR_BLINK) && !(MKFlags & MKFLAG_MOTOR_RUN))
		{
			Output_J16_Set(ParamSet.Config1 & CFG1_MOTOR_OFF_LED1);
		}
		else if ((ParamSet.J16Timing > 250) && (FCParam.J16Timing > 220))
		{
			Output_J16_Set(J16Bitmask & 128);
		}
		else if ((ParamSet.J16Timing > 250) && (FCParam.J16Timing <  10))
		{
			Output_J16_Set(!(J16Bitmask & 128));
		}
		else if (!J16Blinkcount--)
		{
			J16Blinkcount = FCParam.J16Timing - 1;

			if (J16Mask == 1) {
				J16Mask = 128;
			} else {
				J16Mask /= 2;
			}

			Output_J16_Set(J16Mask & J16Bitmask);
		}


		if ((ParamSet.Config1 & CFG1_MOTOR_BLINK) && !(MKFlags & MKFLAG_MOTOR_RUN))
		{
			Output_J17_Set(ParamSet.Config1 & CFG1_MOTOR_OFF_LED2);
		}
		else if ((ParamSet.J17Timing > 250) && (FCParam.J17Timing > 220))
		{
			Output_J17_Set(J17Bitmask & 128);
		}
		else if ((ParamSet.J17Timing > 250) && (FCParam.J17Timing <  10))
		{
			Output_J17_Set(!(J17Bitmask & 128));
		}
		else if (!J17Blinkcount--)
		{
			J17Blinkcount = FCParam.J17Timing - 1;

			if (J17Mask == 1) {
				J17Mask = 128;
			} else {
				J17Mask /= 2;
			}

			Output_J17_Set(J17Mask & J17Bitmask);
		}
	}
}

// LEDs

void LED_Green_Set(bool on)
{
	// for some board versions the signal needs to be inverted
	bool inverted = !(HWInfo_BoardRevision() < Revision_12);

	// sould the bit be set or unset?
	bool set = on != inverted;

	if (set) {
		PORTB |= LED_GREEN;
	} else {
		PORTB &= ~LED_GREEN;
	}
}

void LED_Red_Set(bool on)
{
	// for some board versions the signal needs to be inverted
	bool inverted = !(HWInfo_BoardRevision() == Revision_10 || HWInfo_BoardRevision() == Revision_20);

	// sould the bit be set or unset?
	bool set = on != inverted;

	if (set) {
		PORTB |= LED_RED;
	} else {
		PORTB &= ~LED_RED;
	}
}
