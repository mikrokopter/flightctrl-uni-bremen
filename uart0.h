#ifndef _UART0_H
#define _UART0_H

#include <inttypes.h>



#define RXD_BUFFER_LEN  150

/*!
	\note must be at least 4('#'+Addr+'CmdID'+'\r')+ (80 * 4)/3 = 111 bytes
*/
#define TXD_BUFFER_LEN  150

//! Baud rate of the USART
#define USART0_BAUD  57600



typedef struct
{
	int16_t AngleNick; //! in 0.1 deg
	int16_t AngleRoll; //! in 0.1 deg
	int16_t Heading;   //! in 0.1 deg
	uint8_t reserve[8];
}
#if !defined(__DOXYGEN__)
	__attribute__((packed))
#endif
Data3D_t;

typedef struct
{
	uint8_t  Digital[2];
	uint16_t Analog[32]; //! debug values
}
#if !defined(__DOXYGEN__)
	__attribute__((packed))
#endif
DebugOut_t;

typedef struct
{
	uint8_t Digital[2];
	uint8_t RemoteButtons;
	 int8_t Nick;
	 int8_t Roll;
	 int8_t Yaw;
	uint8_t Gas;
	 int8_t Height;
	uint8_t free;
	uint8_t Frame;
	uint8_t Config;
}
#if !defined(__DOXYGEN__)
	__attribute__((packed))
#endif
ExternControl_t;

typedef struct
{
	int16_t Heading;
}
#if !defined(__DOXYGEN__)
	__attribute__((packed))
#endif
Heading_t;

typedef struct
{
	uint8_t SWMajor;
	uint8_t SWMinor;
	uint8_t ProtoMajor;
	uint8_t ProtoMinor;
	uint8_t SWPatch;
	uint8_t Reserved[5];
}
#if !defined(__DOXYGEN__)
	__attribute__((packed))
#endif
UART_VersionInfo_t;



extern DebugOut_t DebugOut;

extern ExternControl_t ExternControl;

extern uint8_t MotorTest_Active;
extern uint8_t MotorTest[16];

extern uint8_t PcAccess;
extern uint8_t RemotePollDisplayLine;



extern void USART0_Init (void);
extern void USART0_TransmitTxData(void);
extern void USART0_ProcessRxData(void);
extern int16_t uart_putchar(int8_t c);



#endif // _UART0_H
