// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) Holger Buss, Ingo Busker
// + Nur für den privaten Gebrauch
// + www.MikroKopter.com
// + porting the sources to other systems or using the software on other systems (except hardware from www.mikrokopter.de) is not allowed
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten (nicht-kommerziellen) Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder sonstigen Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "timer0.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#include "analog.h"
#include "beep.h"
#include "eeprom.h"
#include "fc.h"
#include "hwinfo.h"
#ifdef USE_KILLAGREG
	#include "mm3.h"
#endif
#ifdef USE_MK3MAG
	#include "mk3mag.h"
#endif
#ifdef USE_RC_SPEKTRUM
	#include "spectrum.h"
#endif



#define USE_BEEPER



volatile uint16_t cntKompass = 0;
volatile uint16_t CountMilliseconds = 0;
volatile bool     UpdateMotor = false;

#ifdef USE_NAVICTRL
	volatile uint8_t SendSPI = 0;
#endif



void Timer_Timer0_Init(void)
{
	uint8_t sreg = SREG;

	// disable all interrupts before reconfiguration
	cli();

	// configure speaker port as output
	if(HWInfo_BoardRevision() == Revision_10)
	{	// Speaker at PD2
		DDRD |= (1<<DDD2);
		PORTD &= ~(1<<PORTD2);
	}
	else
	{	// Speaker at PC7
		DDRC |= (1<<DDC7);
		PORTC &= ~(1<<PORTC7);
	}

	// set PB3 and PB4 as output for the PWM used as offset for the pressure sensor
	DDRB |= (1<<DDB4)|(1<<DDB3);
	PORTB &= ~((1<<PORTB4)|(1<<PORTB3));

	// Timer/Counter 0 Control Register A

	// Waveform Generation Mode is Fast PWM (Bits WGM02 = 0, WGM01 = 1, WGM00 = 1)
	// Clear OC0A on Compare Match, set OC0A at BOTTOM, noninverting PWM (Bits COM0A1 = 1, COM0A0 = 0)
	// Clear OC0B on Compare Match, set OC0B at BOTTOM, (Bits COM0B1 = 1, COM0B0 = 0)
	TCCR0A &= ~((1<<COM0A0)|(1<<COM0B0));
	TCCR0A |= (1<<COM0A1)|(1<<COM0B1)|(1<<WGM01)|(1<<WGM00);

	// Timer/Counter 0 Control Register B

	// set clock divider for timer 0 to SYSKLOCK/8 = 20MHz / 8 = 2.5MHz
	// i.e. the timer increments from 0x00 to 0xFF with an update rate of 2.5 MHz
	// hence the timer overflow interrupt frequency is 2.5 MHz / 256 = 9.765 kHz

	// divider 8 (Bits CS02 = 0, CS01 = 1, CS00 = 0)
	TCCR0B &= ~((1<<FOC0A)|(1<<FOC0B)|(1<<WGM02));
	TCCR0B = (TCCR0B & 0xF8)|(0<<CS02)|(1<<CS01)|(0<<CS00);

	// initialize the Output Compare Register A & B used for PWM generation on port PB3 & PB4
	OCR0A =  0;  // for PB3
	OCR0B = 120; // for PB4

	// init Timer/Counter 0 Register
	TCNT0 = 0;

	// Timer/Counter 0 Interrupt Mask Register
	// enable timer overflow interrupt only
	TIMSK0 &= ~((1<<OCIE0B)|(1<<OCIE0A));
	TIMSK0 |= (1<<TOIE0);

	SREG = sreg;
}

bool Timer_HasTimedOut(timeout_t timeout)
{
	// timed out if (timeout - CountMilliseconds) < 0, i.e. the sign bit is 1
	// check sign bit by bitwise AND with 1<<15 for a 16 bit value
	return (timeout - CountMilliseconds) & _BV(15);
}

timeout_t Timer_TimeoutIn(uint16_t ms)
{
	return CountMilliseconds + ms - 1;
}

void Timer_WaitFor(uint16_t ms)
{
	timeout_t wait_timeout = Timer_TimeoutIn(ms);
	while (!Timer_HasTimedOut(wait_timeout));
}

void Timer_WaitWithMeasurementFor(uint16_t ms)
{
	timeout_t stop_timeout = Timer_TimeoutIn(ms);
	while (!Timer_HasTimedOut(stop_timeout))
	{
		if(ADC_HasFinished())
		{
			ADC_ResetFinished();
			ADC_Enable();
		}
	}
}



ISR(TIMER0_OVF_vect) // 9.765 kHz
{
	static uint8_t cnt_1ms = 1;
	static uint8_t cnt = 0;

	#ifdef USE_NAVICTRL
		if (SendSPI) SendSPI--; // if SendSPI is 0, the transmit of a byte via SPI bus to and from The Navicontrol is done
	#endif

	#ifdef USE_RC_SPEKTRUM
		if (SpektrumTimer) SpektrumTimer--;
	#endif

	if(!cnt--) // every 10th run (9.765kHz/10 = 976Hz ~ 1ms)
	{
		cnt = 9;
		cnt_1ms++;
		cnt_1ms %= 2;
		if (!cnt_1ms) UpdateMotor = true; // every 2nd run (976Hz/2 = 488 Hz ~ 2ms)
		CountMilliseconds++; // increment millisecond counter
	}

	bool Beeper_On = false;
	if(BeepTime)
	{ // beeper on if duration is not over
		BeepTime--; // decrement BeepTime
		Beeper_On = (BeepTime & BeepModulation);
	}
	else
	{ // beeper off if duration is over
		Beeper_On = false;
		BeepModulation = 0xFFFF;
	}

	#ifdef USE_BEEPER
		// if beeper is on
		Beep_Set(Beeper_On);
	#endif

	#ifndef USE_NAVICTRL
		// update compass value if this option is enabled in the settings
		if (ParamSet.Config0 & (CFG0_COMPASS_ACTIVE|CFG0_GPS_ACTIVE))
		{
			#ifdef USE_KILLAGREG
				MM3_Update(); // read out mm3 board
			#endif
			#ifdef USE_MK3MAG
				MK3MAG_Update(); // read out mk3mag pwm
			#endif
		}
	#endif
}
