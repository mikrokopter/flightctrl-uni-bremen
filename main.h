/*!
	\file
	\brief Contains the entry point for the FlightCtrl.

	Here the FlightCtrl sets up its components and enters the main loop.
*/

#ifndef _MAIN_H
#define _MAIN_H

#include <avr/io.h>

#include <stdbool.h>



extern uint16_t FlightMinutes;

extern uint16_t FlightMinutesTotal;

extern uint8_t LowVoltageWarning;



void LipoDetection(bool print);

/*!
	\brief Initializes all subsystems before entering the main event loop.
	\warning Do not call this form your own code.
*/
void Main_Init(void);

/*!
	\brief Containts the main event loop of the FlightCtrl.
	\warning Do not call this form your own code.
*/
void Main_Loop(void);



#endif // _MAIN_H
