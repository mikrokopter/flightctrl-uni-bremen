#ifndef _RC_H
#define _RC_H

#include <avr/io.h>
#include <inttypes.h>

#define J3HIGH    PORTD |= (1<<PORTD5)
#define J3LOW     PORTD &= ~(1<<PORTD5)
#define J3TOGGLE  PORTD ^= (1<<PORTD5)

#define J4HIGH    PORTD |= (1<<PORTD4)
#define J4LOW     PORTD &= ~(1<<PORTD4)
#define J4TOGGLE  PORTD ^= (1<<PORTD4)

#define J5HIGH    PORTD |= (1<<PORTD3)
#define J5LOW     PORTD &= ~(1<<PORTD3)
#define J5TOGGLE  PORTD ^= (1<<PORTD3)

#define PPM_INPUT_ON  TIMSK1 |= (1<<ICIE1)
#define PPM_INPUT_OFF TIMSK1 &= ~(1<<ICIE1)

#define MAX_CHANNELS 15
extern void RC_Init (void);
extern volatile int16_t PPM_in[MAX_CHANNELS];	// the RC-Signal
extern volatile int16_t PPM_diff[MAX_CHANNELS];	// the RC-Signal change per 22.5 ms
extern volatile uint8_t NewPpmData;     // 0 indicates a new recieved PPM Frame
extern volatile uint8_t RC_Quality;     // rc signal quality indicator (0 to 200)
extern volatile uint8_t RC_RSSI;        // Received Signal Strength Indication
extern volatile uint8_t RC_Channels;	// number of received channels

#endif //_RC_H
