// ######################## SPI - FlightCtrl ###################
#ifndef _SPI_H
#define _SPI_H

//#include <util/delay.h>
#include <inttypes.h>


#define SPI_PROTOCOL_COMP   1


#define SPI_CMD_USER        10
#define SPI_CMD_STICK       11
#define SPI_CMD_MISC		12
#define SPI_CMD_PARAMETER1	13
#define SPI_CMD_VERSION		14
#define SPI_CMD_SERVOS		15

typedef struct
{
	uint8_t Sync1;
	uint8_t Sync2;
	uint8_t Command;
	int16_t IntegralNick;
	int16_t IntegralRoll;
	int16_t AccNick;
	int16_t AccRoll;
	int16_t GyroHeading;
	int16_t GyroNick;
	int16_t GyroRoll;
	int16_t GyroYaw;
	union
	{
		int8_t  sByte[12];
		uint8_t Byte[12];
		int16_t Int[6];
		int32_t Long[3];
		float   Float[3];
	} Param;
	uint8_t Chksum;
}
#if !defined(__DOXYGEN__)
__attribute__((packed))
#endif
ToNaviCtrl_t;



#define SPI_CMD_OSD_DATA	100
#define SPI_CMD_GPS_POS		101
#define SPI_CMD_GPS_TARGET	102
#define SPI_KALMAN			103

typedef struct
{
	uint8_t Command;
	int16_t GPSStickNick;
	int16_t GPSStickRoll;
	int16_t GPS_Yaw;
	int16_t CompassHeading;
	int16_t Status;
	uint16_t BeepTime;
	union
	{
		int8_t  sByte[12];
		uint8_t Byte[12];
		int16_t Int[6];
		int32_t Long[3];
		float   Float[3];
	} Param;
	uint8_t Chksum;
}
#if !defined(__DOXYGEN__)
__attribute__((packed))
#endif
FromNaviCtrl_t;


typedef struct
{
	uint8_t Major;
	uint8_t Minor;
	uint8_t Patch;
	uint8_t Compatible;
}
#if !defined(__DOXYGEN__)
__attribute__((packed))
#endif
SPI_VersionInfo_t;


extern ToNaviCtrl_t   			ToNaviCtrl;
extern FromNaviCtrl_t 			FromNaviCtrl;


typedef struct
{
	int8_t	KalmanK;
	int8_t	KalmanMaxDrift;
	int8_t	KalmanMaxFusion;
	uint8_t	SerialDataOkay;
}
#if !defined(__DOXYGEN__)
__attribute__((packed))
#endif
NCData_t;


extern uint8_t NCDataOkay;
extern uint8_t NCSerialDataOkay;
extern int8_t  NCGpsZ;

void SPI_MasterInit(void);
void SPI_StartTransmitPacket(void);
void SPI_TransmitByte(void);



#endif //_SPI_H
