/*!
	\file
	\brief Contains methods to control the ADC and variables with the conversion results.

	The analog-to-digital converter (ADC) is used to convert analog voltages to digital values.
	The values converted by the ADC are:
	- the gyro values for the three axes
	- the accelerator values for the three axes
	- the battery voltage
	- the air pressure

	Typical code might look like:
	\code
		ADC_ResetFinished();
		ADC_Enable();
		while(!ADC_HasFinished()); // not necessarily needed
	\endcode
*/

#ifndef _ANALOG_H
#define _ANALOG_H

#include <stdbool.h>
#include <stdint.h>



#define AIR_PRESSURE_SCALE 18 //! 1 ADC count corresponds approx. to 18 cm
#define AIR_PRESSURE_SEARCH 800
#define AIR_PRESSURE_SEARCH_MIN 750
#define AIR_PRESSURE_SEARCH_MAX 950
#define EXPANDBARO_ADC_SHIFT (-523)	//! adc shift per EXPANDBARO_OPA_OFFSET_STEP
#define EXPANDBARO_OPA_OFFSET_STEP 10
#define HIRES_GYRO_AMPLIFY 8 //! the offset corrected HiResGyro values are a factor of 8 scaled to the AdValues
#define SM_FILTER 16


#if defined(__DOXYGEN__)
	/*!
		Interrupt service routine (ISR) for the ADC.

		\todo explain ADC ISR in detail (for now see ISR code)

		This is called with a frequency of 312.5 kHz (= 2*ADC clock) or 3.2 µs.
		When after 60.8 µs all 19 (0-17 + default) states are processed the interrupt is disabled and the update of further ads is stopped.

		\sa ADC_Enable(), ADC_Init()
	*/
	#define ISR(ADC_vect)
#endif



extern volatile int16_t AdAirPressure; //! ADC value of air pressure measurement
extern volatile int16_t AdValueAccNick;
extern volatile int16_t AdValueAccRoll;
extern volatile int16_t AdValueAccTop;
extern volatile int16_t AdValueAccZ;
extern volatile int16_t AdValueGyroNick;
extern volatile int16_t AdValueGyroRoll;
extern volatile int16_t AdValueGyroYaw;
extern volatile int32_t AirPressure; //! gets less with growing height
extern uint8_t DacOffsetGyroNick;
extern uint8_t DacOffsetGyroRoll;
extern uint8_t DacOffsetGyroYaw;
extern int8_t ExpandBaro;
extern volatile int16_t FilterHiResGyroNick;
extern volatile int16_t FilterHiResGyroRoll;
extern volatile int16_t HiResGyroNick;
extern volatile int16_t HiResGyroRoll;
extern volatile uint16_t MeasurementCounter;
extern uint8_t PressureSensorOffset;
extern volatile int32_t ReadingHeight; //! height according to air pressure (altimeter in steps of 1cm)
extern volatile int16_t ReadingVario;
extern volatile int32_t SumHeight; //! filter for variometer
extern volatile int32_t StartAirPressure;	//! air pressure at ground
extern volatile int16_t UBat;



/*!
	\brief Disables the ADC.

	\sa ADC_Enable(), ADC_HasFinished(), ADC_ResetFinished(), ISR(ADC_vect)
*/
void ADC_Disable(void);

/*!
	\brief Enables the ADC.

	The actual conversions will be done in the \link ISR(ADC_vect) ADC ISR \endlink .
	You will probably want to use ADC_ResetFinished() before enabling the ADC so you can check if the conversion cycle \link ADC_HasFinished() has finished \endlink.

	\note This will have no effect if the ADC has already been enabled.

	\sa ADC_Disable(), ADC_HasFinished(), ADC_ResetFinished(), ISR(ADC_vect)
*/
void ADC_Enable(void);


/*!
	\brief Initialize the ADC.
*/
void ADC_Init(void);

/*!
	\brief Checks whether the ADC has finished the current conversion cycle.

	\return true if a conversion cycle has finished, false otherwise

	\note This should be used in combinantion with ADC_ResetFinished(), reseting before starting a conversion cycle.

	\sa ADC_Enable(), ADC_ResetFinished(), ISR(ADC_vect)
*/
bool ADC_HasFinished(void);

/*!
	\brief Resets the check in ADC_HasFinished() to indicate a running conversion.

	\sa ADC_Enable(), ADC_HasFinished(), ISR(ADC_vect)
*/
void ADC_ResetFinished(void);

void SearchAirPressureOffset(void);
void SearchDacGyroOffset(void);



#endif //_ANALOG_H
