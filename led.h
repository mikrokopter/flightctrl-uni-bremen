/*!
	\file
	\brief Contains variables and functions controlling the MK's LEDs.

	More detailed information about the LEDs can be found in the <a href="http://www.mikrokopter.de/ucwiki/LedBeleuchtung">Mikrokopter Wiki (German)</a>.
*/

#ifndef _LED_H
#define _LED_H

#include <stdbool.h>

#define LED_GREEN  _BV(PORTB1)  //!< Bit controlling the green LED.
#define LED_RED    _BV(PORTB0)  //!< Bit controlling the red LED.

// Output

/*!
	\brief (Un)Sets the output J16.
	\param on Whether the output should be set.
	\todo probably move into a module of their own
*/
void Output_J16_Set(bool on);

/*!
	\brief (Un)Sets the output J17.
	\param on Whether the output should be set.
	\todo probably move into a module of their own
*/
void Output_J17_Set(bool on);

// Lifecycle

/*!
	\brief Initializes the LED control outputs J16, J17.
*/
void LED_Init(void);

/*!
	\note Called in main loop every 2ms.
*/
void LED_Update(void);

// LEDs

/*!
	\brief (Un)Sets the green LED.
	\param on Whether the LED should be on.
*/
void LED_Green_Set(bool on);

/*!
	\brief (Un)Sets the red LED.
	\param on Whether the LED should be on.
*/
void LED_Red_Set(bool on);

#endif //_LED_H
