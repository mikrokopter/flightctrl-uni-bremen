// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) Holger Buss, Ingo Busker
// + Nur für den privaten Gebrauch
// + www.MikroKopter.com
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten (nicht-kommerziellen) Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder sonstigen Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/twi.h>

#include "eeprom.h"
#include "hwinfo.h"
#include "twimaster.h"
#include "fc.h"
#include "motor.h"
#include "analog.h"


volatile uint8_t dac_channel = 0;

uint8_t MissingMotor = 0;

volatile uint8_t TWI_MotorReadIndex  = 0; //! \warning Only use this in the \link ISR(TWI_vect) TWI ISR \endlink .
volatile uint8_t TWI_MotorWriteIndex = 0; //! \warning Only use this in the \link ISR(TWI_vect) TWI ISR \endlink .

/*!
	The current state of the TWI state machine.
	This is used to determine the state of communication on the TWI bus in the \link ISR(TWI_vect) TWI ISR \endlink .
	\warning Do not use this directly.
	\sa TWI_State_Get(), TWI_State_Set(), ISR(TWI_vect)
*/
volatile uint8_t TWI_State = TWI_STATE_MOTOR_TX;

volatile uint16_t TWI_Timeout = 100;



void TWI_Init(void)
{
	uint8_t sreg = SREG;
	cli();

	// SDA is INPUT
	DDRC  &= ~_BV(DDC1);
	// SCL is output
	DDRC |= _BV(DDC0);
	// pull up SDA
	PORTC |= _BV(PORTC0) | _BV(PORTC1);

	// TWI Status Register
	// prescaler 1 (TWPS1 = 0, TWPS0 = 0)
	TWSR &= ~(_BV(TWPS1) | _BV(TWPS0));

	// set TWI Bit Rate Register
	TWBR = ((SYSCLK/SCL_CLOCK)-16)/2;

	TWI_State_Set(TWI_STATE_MOTOR_TX);
	TWI_MotorWriteIndex = 0;
	TWI_MotorReadIndex  = 0;

	// reset all motor data
	for(uint8_t i=0; i < Motor_Count; i++) {
		Motor[i].Setpoint = 0;
		Motor[i].Present  = 0;
		Motor[i].Error    = 0;
		Motor[i].MaxPWM   = 0;
	}

	SREG = sreg;
}

inline uint8_t TWI_ReadByte(void)
{
	return TWDR;
}

void TWI_ReceiveByte(void)
{
	// TWI Control Register
	//   clear TWI interrupt flag (TWINT=1)
	//  enable TWI Acknowledge Bit (TWEA = 1)
	// disable TWI START Condition Bit (TWSTA = 0), MASTER
	// disable TWI STOP Condition Bit (TWSTO = 0)
	// disable TWI Write Collision Flag (TWWC = 0)
	//  enable TWI (TWEN = 1)
	//  enable TWI Interrupt (TWIE = 1)
	TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN) | _BV(TWIE);
}

void TWI_ReceiveLastByte(void)
{
	// TWI Control Register
	//   clear TWI interrupt flag (TWINT=1)
	// disable TWI Acknowledge Bit (TWEA = 0)
	// disable TWI START Condition Bit (TWSTA = 0), MASTER
	// disable TWI STOP Condition Bit (TWSTO = 0)
	// disable TWI Write Collision Flag (TWWC = 0)
	//  enable TWI (TWEN = 1)
	//  enable TWI Interrupt (TWIE = 1)
	TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
}

void TWI_Reset(void)
{
	// stop TWI bus
	TWI_StopAndSet(0);
	TWI_MotorWriteIndex = 0;
	TWI_MotorReadIndex  = 0;

	// TWI Control Register
	//   clear TWI interrupt flag (TWINT=1)
	// disable TWI Acknowledge Bit (TWEA = 0)
	// disable TWI START Condition Bit (TWSTA = 0), MASTER
	// disable TWI STOP Condition Bit (TWSTO = 0)
	// disable TWI Write Collision Flag (TWWC = 0)
	// disable TWI (TWEN = 0)
	// disable TWI Interrupt (TWIE = 0)
	TWCR = _BV(TWINT); // reset to original state incl. interrupt flag reset
	TWAMR = 0; // reset TWI Address Mask Register
	TWAR  = 0; // reset TWI Address Register
	TWDR  = 0; // reset TWI Data Register
	TWSR  = 0; // reset TWI Stats Register
	TWBR  = 0; // reset TWI Bit Rate Register

	TWI_Init();
	TWI_Start(TWI_STATE_MOTOR_TX);
}

void TWI_Start(uint8_t startState)
{
	TWI_State_Set(startState);
	// TWI Control Register
	//   clear TWI interrupt flag (TWINT=1)
	// disable TWI Acknowledge Bit (TWEA = 0)
	//  enable TWI START Condition Bit (TWSTA = 1), MASTER
	// disable TWI STOP Condition Bit (TWSTO = 0)
	// disable TWI Write Collision Flag (TWWC = 0)
	//  enable TWI (TWEN = 1)
	//  enable TWI Interrupt (TWIE = 1)
	TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN) | _BV(TWIE);
}

inline uint8_t TWI_State_Get(void)
{
	return TWI_State;
}

inline void TWI_State_Set(uint8_t newState)
{
	TWI_State = newState;
}

void TWI_Stop()
{
	// TWI Control Register
	//   clear TWI interrupt flag (TWINT=1)
	// disable TWI Acknowledge Bit (TWEA = 0)
	// disable TWI START Condition Bit (TWSTA = 0), no MASTER
	//  enable TWI STOP Condition Bit (TWSTO = 1)
	// disable TWI Write Collision Flag (TWWC = 0)
	//  enable TWI (TWEN = 1)
	// disable TWI Interrupt (TWIE = 0)
	TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);
}

inline void TWI_StopAndSet(uint8_t state)
{
	TWI_State_Set(state);
	TWI_Stop();
}

void TWI_WriteByte(int8_t byte)
{
	// move byte to send into TWI Data Register
	TWDR = byte;
	// TWI Control Register
	//   clear TWI interrupt flag (TWINT=1)
	// disable TWI Acknowledge Bit (TWEA = 0)
	// disable TWI START Condition Bit (TWSTA = 0), no MASTER
	// disable TWI STOP Condition Bit (TWSTO = 0)
	// disable TWI Write Collision Flag (TWWC = 0)
	//  enable TWI (TWEN = 1)
	//  enable TWI Interrupt (TWIE = 1)
	TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWIE);
}



//! \warning Only use in the \link ISR(TWI_vect) TWI ISR \endlink.
inline void TWI_State_ReadMotors_NextMotor(void)
{
	TWI_MotorReadIndex++; // next motor
	if(TWI_MotorReadIndex >= Motor_Count) {
		TWI_MotorReadIndex = 0; // restart reading of first motor if we have reached the last one
	}
}

//! \warning Only use in the \link ISR(TWI_vect) TWI ISR \endlink.
inline void TWI_State_ReadMotors_SelectMotor(void)
{
	// select slave address for receiving data
	TWI_WriteByte(0x53 + (TWI_MotorReadIndex * 2));
}

//! \warning Only use in the \link ISR(TWI_vect) TWI ISR \endlink.
inline void TWI_State_WriteMotors_SelectMotor(void)
{
	// select slave address for transmitting data
	TWI_WriteByte(0x52 + (TWI_MotorWriteIndex * 2));
}

//! \warning Only use in the \link ISR(TWI_vect) TWI ISR \endlink.
inline void TWI_State_WriteMotors_SkipUnused(void)
{
	// skip motor if not used in mixer
	while((Mixer.Motor[TWI_MotorWriteIndex][MIX_GAS] <= 0) && (TWI_MotorWriteIndex < Motor_Count)) {
		TWI_MotorWriteIndex++;
	}
}



ISR(TWI_vect)
{
	//! \todo check whether all used variables are local or volatile

	static uint8_t missingMotorNumber = 0;

	switch (TWI_State++)
	{
	case 0: // TWI_STATE_MOTOR_TX
		/*
			This is invoked in respose to TWI_Start(TWI_STATE_MOTOR_TX).
			It selects the slave (motor) to send data to (if not all motors have received their setpoints yet)
			or selects the slave (motor) to read data from (if all motors have already received their setpoints).
		*/
		// skip motors that are not used in mixer
		TWI_State_WriteMotors_SkipUnused();

		if(TWI_MotorWriteIndex >= Motor_Count) { // writing finished, read now
			TWI_MotorWriteIndex = 0; // reset for next time
			TWI_State_Set(TWI_STATE_MOTOR_RX); // set the next state to be invoked

			TWI_State_ReadMotors_SelectMotor();
		} else {
			TWI_State_WriteMotors_SelectMotor();
		}
		break;
	case 1:
		/*
			This is invoked in respose to the slave selection (TWI_State_WriteMotors_SelectMotor()) in state 0.
			It transmits the actual setpoint for the selected motor.
		*/
		TWI_WriteByte(Motor[TWI_MotorWriteIndex].Setpoint); // transmit rotation rate setpoint
		break;
	case 2:
		/*
			This is invoked in respose to the data transmission (TWI_WriteByte()) in state 1.
			It checks the response of the motor and marks it as missing if it did not respond with an ACK.
			It also stops connection to the current slave (motor), selects the next motor to talk to and does a "repeated start".
		*/
		if(TWSR == TW_MT_DATA_NACK) { // data transmitted, but NACK received
			if(!missingMotorNumber) {
				missingMotorNumber = TWI_MotorWriteIndex + 1;
			}

			// increment error counter and handle overflow
			if(++Motor[TWI_MotorWriteIndex].Error == 0) { // Error overflowed
				Motor[TWI_MotorWriteIndex].Error = 255; // cap error value
			}
		}

		TWI_StopAndSet(TWI_STATE_MOTOR_TX);
		TWI_Timeout = 10;
		TWI_MotorWriteIndex++; // next motor
		TWI_Start(TWI_STATE_MOTOR_TX); // repeated start -> switch slave or switch Master Transmit -> Master Receive
		break;
	case 3: // TWI_STATE_MOTOR_RX
		/*
			This is invoked in respose to the slave selection (TWI_State_ReadMotors_SelectMotor()) in state 0.
			It checks the response of the motor and marks it as missing if it did respond with NACK.
			In that case it also stops connection to the current slave (motor), selects the next motor to talk to and does a repeated start.
			It receives the first byte (motor current).
		*/
		if(TWSR != TW_MR_SLA_ACK) { // SLA+R transmitted, but NACK received
			// no response from the addressed slave (motor) received
			Motor[TWI_MotorReadIndex].Present = 0;
			TWI_State_ReadMotors_NextMotor();
			TWI_StopAndSet(TWI_STATE_MOTOR_TX);
		} else { // SLA+R transmitted and ACK received
			Motor[TWI_MotorReadIndex].Present = ('1' - '-') + TWI_MotorReadIndex; //! \todo the "- '-'" acrobatics seem unnecessary, as it is reverted everywhere it is used
			TWI_ReceiveByte(); // receive 1st byte
		}
		MissingMotor = missingMotorNumber;
		missingMotorNumber = 0;

		break;
	case 4:
		/*
			This is invoked in respose to the TWI_ReceiveByte() call in state 3.
			It reads the previously received byte and receives the second (and final) byte.
		*/
		Motor[TWI_MotorReadIndex].Current = TWI_ReadByte(); // read data
		TWI_ReceiveLastByte(); // send nack for the second byte
		break;
	case 5:
		/*
			This is invoked in respose to the TWI_ReceiveLastByte() call in state 4.
			It reads the previously received byte.
			It then resets the TWI_MotorReadIndex and stops the twi.
		*/
		Motor[TWI_MotorReadIndex].MaxPWM = TWI_ReadByte(); // read data
		TWI_State_ReadMotors_NextMotor();
		TWI_StopAndSet(TWI_STATE_MOTOR_TX);
		break;

	case 7: // TWI_STATE_GYRO_OFFSET_TX // writing Gyro-Offsets
		TWI_WriteByte(0x98); // Address the DAC
		break;

	case 8:
		TWI_WriteByte(0x10 + (dac_channel * 2)); // Select DAC Channel (0x10 = A, 0x12 = B, 0x14 = C)
		break;

	case 9:
		switch(dac_channel)
		{
		case 0:
			TWI_WriteByte(DacOffsetGyroNick); // 1st byte for Channel A
			break;
		case 1:
			TWI_WriteByte(DacOffsetGyroRoll); // 1st byte for Channel B
			break;
		case 2:
			TWI_WriteByte(DacOffsetGyroYaw ); // 1st byte for Channel C
			break;
		}
		break;

	case 10:
		TWI_WriteByte(0x80); // 2nd byte for all channels is 0x80
		break;

	case 11:
		TWI_StopAndSet(TWI_STATE_MOTOR_TX);
		TWI_Timeout = 10;
		// repeat case 7...10 until all DAC Channels are updated
		if(dac_channel < 2)
		{
			dac_channel ++; 	// jump to next channel
			TWI_Start(TWI_STATE_GYRO_OFFSET_TX); 		// start transmission for next channel
		}
		else
		{ // data to last motor send
			dac_channel = 0; // reset dac channel counter
		}
		break;

	default:
		//! \todo determine if this state cna be reached with normal control flow
		/*
			We have entered an illegal state.
			Stop the TWI and reset the variables to sane values.
		*/
		TWI_StopAndSet(TWI_STATE_MOTOR_TX);
		TWI_Timeout = 10;
		TWI_MotorWriteIndex = 0;
		TWI_MotorReadIndex = 0;
	} // switch(TWI_State)
}
