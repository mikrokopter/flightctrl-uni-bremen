/*!
	\file
	\brief Containts methods and structures for dealing with EEPROM access and parameter sets.
*/

#ifndef _EEPROM_H
#define _EEPROM_H

#include <inttypes.h>

#define EEPROM_ADR_PARAM_BEGIN  0
#define PID_PARAM_REVISION      1 // byte
#define PID_ACTIVE_SET          2 // byte
#define PID_PRESSURE_OFFSET     3 // byte

#define PID_ACC_NICK            4 // word
#define PID_ACC_ROLL            6 // word
#define PID_ACC_TOP             8 // word

#define PID_FLIGHT_MINUTES_TOTAL  10 // word
#define PID_FLIGHT_MINUTES        14 // word

#ifdef USE_KILLAGREG
#define PID_MM3_X_OFF    31 // byte
#define PID_MM3_Y_OFF    32 // byte
#define PID_MM3_Z_OFF    33 // byte
#define PID_MM3_X_RANGE  34 // word
#define PID_MM3_Y_RANGE  36 // word
#define PID_MM3_Z_RANGE  38 // word
#endif

#define EEPROM_ADR_CHANNELS  80 // 8 bytes

#define EEPROM_ADR_PARAMSET_LENGTH  98 // word
#define EEPROM_ADR_PARAMSET_BEGIN  100

#define EEPROM_ADR_MIXER_TABLE  1000 // 1000 - 1076

//! \todo make this an enum
#define CFG0_AIRPRESS_SENSOR  0x01 //!< Bit mask for \ref paramset_t::Config0
#define CFG0_HEIGHT_SWITCH    0x02 //!< Bit mask for \ref paramset_t::Config0
#define CFG0_HEADING_HOLD     0x04 //!< Bit mask for \ref paramset_t::Config0
#define CFG0_COMPASS_ACTIVE   0x08 //!< Bit mask for \ref paramset_t::Config0
#define CFG0_COMPASS_FIX      0x10 //!< Bit mask for \ref paramset_t::Config0
#define CFG0_GPS_ACTIVE       0x20 //!< Bit mask for \ref paramset_t::Config0
#define CFG0_AXIS_COUPLING_ACTIVE  0x40 //!< Bit mask for \ref paramset_t::Config0
#define CFG0_ROTARY_RATE_LIMITER   0x80 //!< Bit mask for \ref paramset_t::Config0

//! \todo make this an enum
#define CFG1_LOOP_UP         0x01 //!< Bit mask for \ref paramset_t::Config1
#define CFG1_LOOP_DOWN       0x02 //!< Bit mask for \ref paramset_t::Config1
#define CFG1_LOOP_LEFT       0x04 //!< Bit mask for \ref paramset_t::Config1
#define CFG1_LOOP_RIGHT      0x08 //!< Bit mask for \ref paramset_t::Config1
#define CFG1_MOTOR_BLINK     0x10 //!< Bit mask for \ref paramset_t::Config1
#define CFG1_MOTOR_OFF_LED1  0x20 //!< Bit mask for \ref paramset_t::Config1
#define CFG1_MOTOR_OFF_LED2  0x40 //!< Bit mask for \ref paramset_t::Config1
#define CFG1_RES4            0x80 //!< Bit mask for \ref paramset_t::Config1

//! \todo make this an enum
#define CFG2_HEIGHT_LIMIT  0x01 //!< Bit mask for \ref paramset_t::Config2
#define CFG2_VARIO_BEEP    0x02 //!< Bit mask for \ref paramset_t::Config2
#define CFG2_SENSITIVE_RC  0x04 //!< Bit mask for \ref paramset_t::Config2
#define CFG2_RES1          0x08 //!< Bit mask for \ref paramset_t::Config2
#define CFG2_RES2          0x10 //!< Bit mask for \ref paramset_t::Config2
#define CFG2_RES3          0x20 //!< Bit mask for \ref paramset_t::Config2
#define CFG2_RES4          0x40 //!< Bit mask for \ref paramset_t::Config2
#define CFG2_RES5          0x80 //!< Bit mask for \ref paramset_t::Config2

// defines for looking up ParamSet.ChannelAssignment
//! \todo make this an enum
#define CH_NICK   0 //!< Channel for nick.  \sa paramset_t.ChannelAssignment
#define CH_ROLL   1 //!< Channel for roll.  \sa paramset_t.ChannelAssignment
#define CH_GAS    2 //!< Channel for gas.  \sa paramset_t.ChannelAssignment
#define CH_YAW    3 //!< Channel for yaw.  \sa paramset_t.ChannelAssignment
#define CH_POTI1  4 //!< Channel for poti1.  \sa paramset_t.ChannelAssignment
#define CH_POTI2  5 //!< Channel for poti2.  \sa paramset_t.ChannelAssignment
#define CH_POTI3  6 //!< Channel for poti3.  \sa paramset_t.ChannelAssignment
#define CH_POTI4  7 //!< Channel for poti4.  \sa paramset_t.ChannelAssignment

/*!
	\brief The current EEPROM data (layout) revision.

	\warning For data compatibility reasons this should be incremented, when \ref paramset_t gets changed.
*/
#define EEPARAM_REVISION	80

/*!
	\brief The current Mixer data (layout) revision.

	\warning For data compatibility reasons this should be incremented, when \ref MixerTable_t gets changed.
*/
#define EEMIXER_REVISION	 1



/*!
	\brief Struct for storing parameter set data.

	\note values above 250 represent poti1 to poti4.

	\warning For data compatibility reasons \ref EEPARAM_REVISION should be incremented, when this gets changed.
*/
typedef struct
{
	uint8_t ChannelAssignment[8]; // see upper defines for details
	uint8_t Config0;           // see upper defines for bitcoding
	uint8_t HeightMinGas;      // Value: 0-100
	uint8_t HeightD;           // Value: 0-250
	uint8_t MaxHeight;         // Value: 0-32
	uint8_t HeightP;           // Value: 0-32
	uint8_t Height_Gain;       // Value: 0-50
	uint8_t Height_ACC_Effect; // Value: 0-250
	uint8_t Height_HoverBand;  // Value: 0-250
	uint8_t Height_GPS_Z;      // Value: 0-250
	uint8_t Height_StickNeutralPoint; // Value : 0-250
	uint8_t StickP;            // Value: 1-6
	uint8_t StickD;            // Value: 0-64
	uint8_t StickYawP;         // Value: 1-20
	uint8_t GasMin;            // Value: 0-32
	uint8_t GasMax;            // Value: 33-250
	uint8_t GyroAccFactor;     // Value: 1-64
	uint8_t CompassYawEffect;  // Value: 0-32
	uint8_t GyroP;             // Value: 10-250
	uint8_t GyroI;             // Value: 0-250
	uint8_t GyroD;             // Value: 0-250
	uint8_t GyroYawP;          // Value: 10-250
	uint8_t GyroYawI;          // Value: 0-250
	uint8_t LowVoltageWarning; // Value: 0-250
	uint8_t EmergencyGas;      // Value: 0-250  Gaswert bei Empängsverlust
	uint8_t EmergencyGasDuration; // Value: 0-250  Zeitbis auf EmergencyGas geschaltet wird, wg. Rx-Problemen
	uint8_t UfoArrangement;    // x or + Formation
	uint8_t IFactor;           // Value: 0-250
	uint8_t UserParam1;        // Value: 0-250
	uint8_t UserParam2;        // Value: 0-250
	uint8_t UserParam3;        // Value: 0-250
	uint8_t UserParam4;        // Value: 0-250
	uint8_t ServoNickControl;  // Value: 0-250  Stellung des Nick Servos
	uint8_t ServoNickComp;     // Value: 0-250  Einfluss Nick-Gyro/Servo
	uint8_t ServoNickMin;      // Value: 0-250  Anschlag
	uint8_t ServoNickMax;      // Value: 0-250  Anschlag
	uint8_t ServoRollControl;  // Value: 0-250  Stellung des Roll Servos
	uint8_t ServoRollComp;     // Value: 0-250  Einfluss Roll-Gyro/Servo
	uint8_t ServoRollMin;      // Value: 0-250  Anschlag
	uint8_t ServoRollMax;      // Value: 0-250  Anschlag
	uint8_t ServoRefresh;      // Value: 0-250  Refreshrate of servo pwm output
	uint8_t LoopGasLimit;      // Value: 0-250  max. Gas während Looping
	uint8_t LoopThreshold;     // Value: 0-250  Schwelle für Stickausschlag
	uint8_t LoopHysteresis;    // Value: 0-250  Hysterese für Stickausschlag
	// Axis coupling
	uint8_t AxisCoupling1;     // Value: 0-250  Faktor, mit dem Yaw die Achsen Roll und Nick koppelt (NickRollMitkopplung)
	uint8_t AxisCoupling2;     // Value: 0-250  Faktor, mit dem Nick und Roll verkoppelt werden
	uint8_t AxisCouplingYawCorrection; // Value: 0-250  Faktor, mit dem Nick und Roll verkoppelt werden
	uint8_t AngleTurnOverNick; // Value: 0-250  180°-Punkt
	uint8_t AngleTurnOverRoll; // Value: 0-250  180°-Punkt
	uint8_t GyroAccTrim;       // 1/k  (Koppel_ACC_Wirkung)
	uint8_t DriftComp;         // limit for gyrodrift compensation
	uint8_t DynamicStability;  // PID limit for Attitude controller
	uint8_t UserParam5;        // Value: 0-250
	uint8_t UserParam6;        // Value: 0-250
	uint8_t UserParam7;        // Value: 0-250
	uint8_t UserParam8;        // Value: 0-250
	// Output
	uint8_t J16Bitmask;         // for the J16 Output
	uint8_t J16Timing;          // for the J16 Output
	uint8_t J17Bitmask;         // for the J17 Output
	uint8_t J17Timing;          // for the J17 Output
	uint8_t J16Bitmask_Warning; // for the J16 Outout
	uint8_t J17Bitmask_Warning; // for the J17 Outout
	// NaviCtrl
	uint8_t NaviGpsModeControl; // Parameters for the Naviboard
	uint8_t NaviGpsGain;        // overall gain for GPS-PID controller
	uint8_t NaviGpsP;           // P gain for GPS-PID controller
	uint8_t NaviGpsI;           // I gain for GPS-PID controller
	uint8_t NaviGpsD;           // D gain for GPS-PID controller
	uint8_t NaviGpsPLimit;      // P limit for GPS-PID controller
	uint8_t NaviGpsILimit;      // I limit for GPS-PID controller
	uint8_t NaviGpsDLimit;      // D limit for GPS-PID controller
	uint8_t NaviGpsACC;         // ACC gain for GPS-PID controller
	uint8_t NaviGpsMinSat;      // number of sattelites neccesary for GPS functions
	uint8_t NaviStickThreshold; // activation threshild for detection of manual stick movements
	uint8_t NaviWindCorrection;	// streng of wind course correction
	uint8_t NaviSpeedCompensation; // D gain fefore position hold login
	uint8_t NaviOperatingRadius; // Radius limit in m around start position for GPS flights
	uint8_t NaviAngleLimitation; // limitation of attitude angle controlled by the gps algorithm
	uint8_t NaviPHLoginTime;     // position hold logintimeout
	// extern control
	uint8_t ExternalControl; // for serial control
	// config
	uint8_t Config1;         // see upper defines for bitcoding
	uint8_t ServoCompInvert; // Bitfield: 0x01 = Nick invert, 0x02 = Roll invert // WICHTIG!!! am Ende lassen
	uint8_t Config2;         // see upper defines for bitcoding
	int8_t Name[12];
} paramset_t;

//! The size of a parameter set in bytes.
#define  PARAMSET_STRUCT_LEN  sizeof(paramset_t)



//! The current set of parameters
extern paramset_t ParamSet;



extern void ParamSet_Init(void);
extern void ParamSet_ReadFromEEProm(uint8_t setnumber);
extern void ParamSet_WriteToEEProm(uint8_t setnumber);
extern uint8_t GetActiveParamSet(void);
extern void SetActiveParamSet(uint8_t setnumber);

extern uint8_t MixerTable_ReadFromEEProm(void);
extern uint8_t MixerTable_WriteToEEProm(void);


extern uint8_t GetParamByte(uint16_t param_id);
extern void SetParamByte(uint16_t param_id, uint8_t value);
extern uint16_t GetParamWord(uint16_t param_id);
extern void SetParamWord(uint16_t param_id, uint16_t value);


#endif //_EEPROM_H
