// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) Holger Buss, Ingo Busker
// + Nur für den privaten Gebrauch
// + porting the sources to other systems or using the software on other systems (except hardware from www.mikrokopter.de) is not allowed
// + www.MikroKopter.com
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten (nicht-kommerziellen) Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder sonstigen Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Contant Values
// + 0-250 -> normale Values
// + 251 -> Poti1
// + 252 -> Poti2
// + 253 -> Poti3
// + 254 -> Poti4
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef EEMEM
#define EEMEM __attribute__ ((section (".eeprom")))
#endif


#include <avr/eeprom.h>
#include <string.h>
#include "eeprom.h"
#include "hwinfo.h"
#include "printf_P.h"
#include "led.h"
#include "fc.h"
#include "motor.h"


// byte array in eeprom
uint8_t EEPromArray[E2END+1] EEMEM;

paramset_t		ParamSet;


/***************************************************/
/*    Default Values for parameter set 1           */
/***************************************************/
void ParamSet_DefaultSet1(void) // sport
{
	if(HWInfo_BoardRevision() >= Revision_20)
	{
		ParamSet.GyroD = 5;
		ParamSet.DriftComp = 0;
		ParamSet.GyroAccFactor = 27;
		ParamSet.AngleTurnOverNick = 78;
		ParamSet.AngleTurnOverRoll = 78;
	}
	else
	{
		ParamSet.GyroD = 3;
		ParamSet.DriftComp = 32;
		ParamSet.GyroAccFactor = 30;
		ParamSet.AngleTurnOverNick = 85;
		ParamSet.AngleTurnOverRoll = 85;
	}
	ParamSet.ChannelAssignment[CH_GAS]	 = 1;
	ParamSet.ChannelAssignment[CH_ROLL]  = 2;
	ParamSet.ChannelAssignment[CH_NICK]  = 3;
	ParamSet.ChannelAssignment[CH_YAW]   = 4;
	ParamSet.ChannelAssignment[CH_POTI1] = 5;
	ParamSet.ChannelAssignment[CH_POTI2] = 6;
	ParamSet.ChannelAssignment[CH_POTI3] = 7;
	ParamSet.ChannelAssignment[CH_POTI4] = 8;
	ParamSet.Config0 = CFG0_AXIS_COUPLING_ACTIVE | CFG0_COMPASS_ACTIVE | CFG0_GPS_ACTIVE | CFG0_HEIGHT_SWITCH;//CFG0_HEIGHT_CONTROL | CFG0_COMPASS_FIX;
	ParamSet.Config1 = 0;
	ParamSet.Config2 = CFG2_HEIGHT_LIMIT;//|CFG2_SENSITIVE_RC;
	ParamSet.HeightMinGas = 30;
	ParamSet.MaxHeight     = 251;
	ParamSet.HeightP      = 10;
	ParamSet.HeightD  = 30;
	ParamSet.Height_ACC_Effect = 30;
	ParamSet.Height_HoverBand = 5;
	ParamSet.Height_GPS_Z = 64;
	ParamSet.Height_StickNeutralPoint = 0; // Value : 0-250 (0 = Hoover-Estimation)
	ParamSet.Height_Gain = 20;
	ParamSet.StickP = 14;
	ParamSet.StickD = 16;
	ParamSet.StickYawP = 12;
	ParamSet.GasMin = 8;
	ParamSet.GasMax = 230;
	ParamSet.CompassYawEffect = 128;
	ParamSet.GyroP = 80;
	ParamSet.GyroI = 150;
	ParamSet.GyroYawP = 80;
	ParamSet.GyroYawI = 150;
	ParamSet.LowVoltageWarning = 33;  // automatic cell detection for values < 50
	ParamSet.EmergencyGas = 35;
	ParamSet.EmergencyGasDuration = 30;
	ParamSet.UfoArrangement = 0;
	ParamSet.IFactor = 32;
	ParamSet.UserParam1 = 0;
	ParamSet.UserParam2 = 0;
	ParamSet.UserParam3 = 0;
	ParamSet.UserParam4 = 0;
	ParamSet.UserParam5 = 0;
	ParamSet.UserParam6 = 0;
	ParamSet.UserParam7 = 0;
	ParamSet.UserParam8 = 0;
	ParamSet.ServoCompInvert = 1;
	ParamSet.ServoRefresh = 6;
	ParamSet.ServoNickControl = 100;
	ParamSet.ServoNickComp = 40;
	ParamSet.ServoNickMin = 0;
	ParamSet.ServoNickMax = 250;
	ParamSet.ServoRollControl = 100;
	ParamSet.ServoRollComp = 40;
	ParamSet.ServoRollMin = 0;
	ParamSet.ServoRollMax = 250;
	ParamSet.LoopGasLimit = 50;
	ParamSet.LoopThreshold = 90;
	ParamSet.LoopHysteresis = 50;
	ParamSet.AxisCoupling1 = 90;
	ParamSet.AxisCoupling2 = 80;
	ParamSet.AxisCouplingYawCorrection = 1;
	ParamSet.GyroAccTrim = 16;
	ParamSet.DynamicStability = 100;
	ParamSet.J16Bitmask = 95;
	ParamSet.J17Bitmask = 243;
	ParamSet.J16Bitmask_Warning = 0xAA;
	ParamSet.J17Bitmask_Warning = 0xAA;
	ParamSet.J16Timing = 15;
	ParamSet.J17Timing = 15;
	ParamSet.NaviGpsModeControl = 252;
	ParamSet.NaviGpsGain = 100;
	ParamSet.NaviGpsP = 90;
	ParamSet.NaviGpsI = 90;
	ParamSet.NaviGpsD = 90;
	ParamSet.NaviGpsPLimit = 75;
	ParamSet.NaviGpsILimit = 75;
	ParamSet.NaviGpsDLimit = 75;
	ParamSet.NaviGpsACC = 0;
	ParamSet.NaviGpsMinSat = 6;
	ParamSet.NaviStickThreshold = 8;
	ParamSet.NaviWindCorrection = 90;
	ParamSet.NaviSpeedCompensation = 30;
	ParamSet.NaviOperatingRadius = 100;
	ParamSet.NaviAngleLimitation = 100;
	ParamSet.NaviPHLoginTime = 4;
	memcpy(ParamSet.Name, "Sport\0",6);
}


/***************************************************/
/*    Default Values for parameter set 2           */
/***************************************************/
void ParamSet_DefaultSet2(void) // normal
{
	if(HWInfo_BoardRevision() >= Revision_20)
	{
		ParamSet.GyroD = 5;
		ParamSet.DriftComp = 0;
		ParamSet.GyroAccFactor = 27;
		ParamSet.AngleTurnOverNick = 78;
		ParamSet.AngleTurnOverRoll = 78;
	}
	else
	{
		ParamSet.GyroD = 3;
		ParamSet.DriftComp = 32;
		ParamSet.GyroAccFactor = 30;
		ParamSet.AngleTurnOverNick = 85;
		ParamSet.AngleTurnOverRoll = 85;
	}
	ParamSet.ChannelAssignment[CH_GAS]   = 1;
	ParamSet.ChannelAssignment[CH_ROLL]  = 2;
	ParamSet.ChannelAssignment[CH_NICK]  = 3;
	ParamSet.ChannelAssignment[CH_YAW]   = 4;
	ParamSet.ChannelAssignment[CH_POTI1] = 5;
	ParamSet.ChannelAssignment[CH_POTI2] = 6;
	ParamSet.ChannelAssignment[CH_POTI3] = 7;
	ParamSet.ChannelAssignment[CH_POTI4] = 8;
	ParamSet.Config0 = CFG0_AXIS_COUPLING_ACTIVE | CFG0_COMPASS_ACTIVE | CFG0_GPS_ACTIVE | CFG0_HEIGHT_SWITCH;//CFG0_HEIGHT_CONTROL | CFG0_COMPASS_FIX;
	ParamSet.Config1 = 0;
	ParamSet.Config2 = CFG2_HEIGHT_LIMIT;//|CFG2_SENSITIVE_RC;
	ParamSet.HeightMinGas = 30;
	ParamSet.MaxHeight     = 251;
	ParamSet.HeightP      = 10;
	ParamSet.HeightD  = 30;
	ParamSet.Height_ACC_Effect = 30;
	ParamSet.Height_HoverBand = 5;
	ParamSet.Height_GPS_Z = 64;
	ParamSet.Height_StickNeutralPoint = 0; // Value : 0-250 (0 = Hoover-Estimation)
	ParamSet.Height_Gain = 15;
	ParamSet.StickP = 10;
	ParamSet.StickD = 16;
	ParamSet.StickYawP = 6;
	ParamSet.GasMin = 8;
	ParamSet.GasMax = 230;
	ParamSet.CompassYawEffect = 128;
	ParamSet.GyroP = 90;
	ParamSet.GyroI = 120;
	ParamSet.GyroYawP = 90;
	ParamSet.GyroYawI = 120;
	ParamSet.LowVoltageWarning = 33; // auto cell detection for values < 50
	ParamSet.EmergencyGas = 35;
	ParamSet.EmergencyGasDuration = 30;
	ParamSet.UfoArrangement = 0;
	ParamSet.IFactor = 32;
	ParamSet.UserParam1 = 0;
	ParamSet.UserParam2 = 0;
	ParamSet.UserParam3 = 0;
	ParamSet.UserParam4 = 0;
	ParamSet.UserParam5 = 0;
	ParamSet.UserParam6 = 0;
	ParamSet.UserParam7 = 0;
	ParamSet.UserParam8 = 0;
	ParamSet.ServoCompInvert = 1;
	ParamSet.ServoRefresh = 6;
	ParamSet.ServoNickControl = 100;
	ParamSet.ServoNickComp = 40;
	ParamSet.ServoNickMin = 0;
	ParamSet.ServoNickMax = 250;
	ParamSet.ServoRollControl = 100;
	ParamSet.ServoRollComp = 40;
	ParamSet.ServoRollMin = 0;
	ParamSet.ServoRollMax = 250;
	ParamSet.LoopGasLimit = 50;
	ParamSet.LoopThreshold = 90;
	ParamSet.LoopHysteresis = 50;
	ParamSet.AxisCoupling1 = 90;
	ParamSet.AxisCoupling2 = 80;
	ParamSet.AxisCouplingYawCorrection = 60;
	ParamSet.GyroAccTrim = 32;
	ParamSet.DynamicStability = 75;
	ParamSet.J16Bitmask = 95;
	ParamSet.J17Bitmask = 243;
	ParamSet.J16Bitmask_Warning = 0xAA;
	ParamSet.J17Bitmask_Warning = 0xAA;
	ParamSet.J16Timing = 20;
	ParamSet.J17Timing = 20;
	ParamSet.NaviGpsModeControl = 252;
	ParamSet.NaviGpsGain = 100;
	ParamSet.NaviGpsP = 90;
	ParamSet.NaviGpsI = 90;
	ParamSet.NaviGpsD = 90;
	ParamSet.NaviGpsPLimit = 75;
	ParamSet.NaviGpsILimit = 75;
	ParamSet.NaviGpsDLimit = 75;
	ParamSet.NaviGpsACC = 0;
	ParamSet.NaviGpsMinSat = 6;
	ParamSet.NaviStickThreshold = 8;
	ParamSet.NaviWindCorrection = 90;
	ParamSet.NaviSpeedCompensation = 30;
	ParamSet.NaviOperatingRadius = 100;
	ParamSet.NaviAngleLimitation = 100;
	ParamSet.NaviPHLoginTime = 4;
	memcpy(ParamSet.Name, "Normal\0", 7);
}


/***************************************************/
/*    Default Values for parameter set 3           */
/***************************************************/
void ParamSet_DefaultSet3(void) // beginner
{
	if(HWInfo_BoardRevision() >= Revision_20)
	{
		ParamSet.GyroD = 5;
		ParamSet.DriftComp = 0;
		ParamSet.GyroAccFactor = 27;         // Value : 1-64
		ParamSet.AngleTurnOverNick = 78;
		ParamSet.AngleTurnOverRoll = 78;
	}
	else
	{
		ParamSet.GyroD = 3;
		ParamSet.DriftComp = 32;
		ParamSet.GyroAccFactor = 30;         // Value : 1-64
		ParamSet.AngleTurnOverNick = 85;
		ParamSet.AngleTurnOverRoll = 85;
	}
	ParamSet.ChannelAssignment[CH_GAS]   = 1;
	ParamSet.ChannelAssignment[CH_ROLL]  = 2;
	ParamSet.ChannelAssignment[CH_NICK]  = 3;
	ParamSet.ChannelAssignment[CH_YAW]   = 4;
	ParamSet.ChannelAssignment[CH_POTI1] = 5;
	ParamSet.ChannelAssignment[CH_POTI2] = 6;
	ParamSet.ChannelAssignment[CH_POTI3] = 7;
	ParamSet.ChannelAssignment[CH_POTI4] = 8;
	ParamSet.Config0 = CFG0_AXIS_COUPLING_ACTIVE | CFG0_COMPASS_ACTIVE | CFG0_GPS_ACTIVE | CFG0_HEIGHT_SWITCH;//CFG0_HEIGHT_CONTROL | CFG0_COMPASS_FIX | CFG0_ROTARY_RATE_LIMITER;
	ParamSet.Config1 = 0;
	ParamSet.Config2 = CFG2_HEIGHT_LIMIT;//|CFG2_SENSITIVE_RC;
	ParamSet.HeightMinGas = 30;
	ParamSet.MaxHeight     = 251;
	ParamSet.HeightP      = 10;
	ParamSet.HeightD  = 30;
	ParamSet.Height_ACC_Effect = 30;
	ParamSet.Height_HoverBand = 5;
	ParamSet.Height_GPS_Z = 64;
	ParamSet.Height_StickNeutralPoint = 0; // Value : 0-250 (0 = Hoover-Estimation)
	ParamSet.Height_Gain = 15;
	ParamSet.StickP = 8;
	ParamSet.StickD = 16;
	ParamSet.StickYawP  = 6;
	ParamSet.GasMin = 8;
	ParamSet.GasMax = 230;
	ParamSet.CompassYawEffect = 128;
	ParamSet.GyroP = 100;
	ParamSet.GyroI = 120;
	ParamSet.GyroYawP = 100;
	ParamSet.GyroYawI = 120;
	ParamSet.LowVoltageWarning = 33;  // auto cell detection for values < 50
	ParamSet.EmergencyGas = 35;
	ParamSet.EmergencyGasDuration = 20;
	ParamSet.UfoArrangement = 0;
	ParamSet.IFactor = 16;
	ParamSet.UserParam1 = 0;
	ParamSet.UserParam2 = 0;
	ParamSet.UserParam3 = 0;
	ParamSet.UserParam4 = 0;
	ParamSet.UserParam5 = 0;
	ParamSet.UserParam6 = 0;
	ParamSet.UserParam7 = 0;
	ParamSet.UserParam8 = 0;
	ParamSet.ServoCompInvert = 1;
	ParamSet.ServoRefresh = 6;
	ParamSet.ServoNickControl = 100;
	ParamSet.ServoNickComp = 40;
	ParamSet.ServoNickMin = 0;
	ParamSet.ServoNickMax = 250;
	ParamSet.ServoRollControl = 100;
	ParamSet.ServoRollComp = 40;
	ParamSet.ServoRollMin = 0;
	ParamSet.ServoRollMax = 250;
	ParamSet.LoopGasLimit = 50;
	ParamSet.LoopThreshold = 90;
	ParamSet.LoopHysteresis = 50;
	ParamSet.AxisCoupling1 = 90;
	ParamSet.AxisCoupling2 = 80;
	ParamSet.AxisCouplingYawCorrection = 70;
	ParamSet.GyroAccTrim = 32;
	ParamSet.DynamicStability = 50;
	ParamSet.J16Bitmask = 95;
	ParamSet.J17Bitmask = 243;
	ParamSet.J16Bitmask_Warning = 0xAA;
	ParamSet.J17Bitmask_Warning = 0xAA;
	ParamSet.J16Timing = 30;
	ParamSet.J17Timing = 30;
	ParamSet.NaviGpsModeControl = 252;
	ParamSet.NaviGpsGain = 100;
	ParamSet.NaviGpsP = 90;
	ParamSet.NaviGpsI = 90;
	ParamSet.NaviGpsD = 90;
	ParamSet.NaviGpsPLimit = 75;
	ParamSet.NaviGpsILimit = 75;
	ParamSet.NaviGpsDLimit = 75;
	ParamSet.NaviGpsACC = 0;
	ParamSet.NaviGpsMinSat = 6;
	ParamSet.NaviStickThreshold = 8;
	ParamSet.NaviWindCorrection = 90;
	ParamSet.NaviSpeedCompensation = 30;
	ParamSet.NaviOperatingRadius = 100;
	ParamSet.NaviAngleLimitation = 100;
	ParamSet.NaviPHLoginTime = 4;
	memcpy(ParamSet.Name, "Beginner\0", 9);
}

/***************************************************/
/*       Read Parameter from EEPROM as byte        */
/***************************************************/
uint8_t GetParamByte(uint16_t param_id)
{
	return eeprom_read_byte(&EEPromArray[EEPROM_ADR_PARAM_BEGIN + param_id]);
}

/***************************************************/
/*       Write Parameter to EEPROM as byte         */
/***************************************************/
void SetParamByte(uint16_t param_id, uint8_t value)
{
	eeprom_write_byte(&EEPromArray[EEPROM_ADR_PARAM_BEGIN + param_id], value);
}

/***************************************************/
/*       Read Parameter from EEPROM as word        */
/***************************************************/
uint16_t GetParamWord(uint16_t param_id)
{
	return eeprom_read_word((uint16_t *) &EEPromArray[EEPROM_ADR_PARAM_BEGIN + param_id]);
}

/***************************************************/
/*       Write Parameter to EEPROM as word         */
/***************************************************/
void SetParamWord(uint16_t param_id, uint16_t value)
{
	eeprom_write_word((uint16_t *) &EEPromArray[EEPROM_ADR_PARAM_BEGIN + param_id], value);
}

/***************************************************/
/*       Read Parameter Set from EEPROM            */
/***************************************************/
// number [1..5]
void ParamSet_ReadFromEEProm(uint8_t setnumber)
{
	if((1 > setnumber) || (setnumber > 5)) setnumber = 3;
	eeprom_read_block((uint8_t *) &ParamSet.ChannelAssignment[0], &EEPromArray[EEPROM_ADR_PARAMSET_BEGIN + PARAMSET_STRUCT_LEN * (setnumber - 1)], PARAMSET_STRUCT_LEN);
	LED_Init();
}

/***************************************************/
/*        Write Parameter Set to EEPROM            */
/***************************************************/
// number [1..5]
void ParamSet_WriteToEEProm(uint8_t setnumber)
{
	if(setnumber > 5) setnumber = 5;
	if(setnumber < 1) return;
	eeprom_write_block((uint8_t *) &ParamSet.ChannelAssignment[0], &EEPromArray[EEPROM_ADR_PARAMSET_BEGIN + PARAMSET_STRUCT_LEN * (setnumber - 1)], PARAMSET_STRUCT_LEN);
	eeprom_write_word((uint16_t *) &EEPromArray[EEPROM_ADR_PARAMSET_LENGTH], PARAMSET_STRUCT_LEN);
	eeprom_write_block( &ParamSet.ChannelAssignment[0], &EEPromArray[EEPROM_ADR_CHANNELS], 8); // backup the first 8 bytes that is the rc channel mapping
	// set this parameter set to active set
	SetActiveParamSet(setnumber);
	LED_Init();
}

/***************************************************/
/*          Read MixerTable from EEPROM            */
/***************************************************/
uint8_t MixerTable_ReadFromEEProm(void)
{
	if(eeprom_read_byte(&EEPromArray[EEPROM_ADR_MIXER_TABLE]) == EEMIXER_REVISION)
	{
		eeprom_read_block((uint8_t *) &Mixer, &EEPromArray[EEPROM_ADR_MIXER_TABLE], sizeof(Mixer));
		return 1;
	}
	else return 0;
}

/***************************************************/
/*          Write Mixer Table to EEPROM            */
/***************************************************/
uint8_t MixerTable_WriteToEEProm(void)
{
	if(Mixer.Revision == EEMIXER_REVISION)
	{
		eeprom_write_block((uint8_t *) &Mixer, &EEPromArray[EEPROM_ADR_MIXER_TABLE], sizeof(Mixer));
		return 1;
	}
	else return 0;
}

/***************************************************/
/*    Default Values for Mixer Table               */
/***************************************************/
void MixerTable_Default(void) // Quadro
{
	uint8_t i;

	Mixer.Revision = EEMIXER_REVISION;
	// clear mixer table
	for(i = 0; i < 16; i++)
	{
		Mixer.Motor[i][MIX_GAS]  = 0;
		Mixer.Motor[i][MIX_NICK] = 0;
		Mixer.Motor[i][MIX_ROLL] = 0;
		Mixer.Motor[i][MIX_YAW]  = 0;
	}
	// default = Quadro
	Mixer.Motor[0][MIX_GAS] = 64; Mixer.Motor[0][MIX_NICK] = +64; Mixer.Motor[0][MIX_ROLL] =   0; Mixer.Motor[0][MIX_YAW] = +64;
	Mixer.Motor[1][MIX_GAS] = 64; Mixer.Motor[1][MIX_NICK] = -64; Mixer.Motor[1][MIX_ROLL] =   0; Mixer.Motor[1][MIX_YAW] = +64;
	Mixer.Motor[2][MIX_GAS] = 64; Mixer.Motor[2][MIX_NICK] =   0; Mixer.Motor[2][MIX_ROLL] = -64; Mixer.Motor[2][MIX_YAW] = -64;
	Mixer.Motor[3][MIX_GAS] = 64; Mixer.Motor[3][MIX_NICK] =   0; Mixer.Motor[3][MIX_ROLL] = +64; Mixer.Motor[3][MIX_YAW] = -64;
	memcpy(Mixer.Name, "Quadro\0", 7);
}


/***************************************************/
/*       Get active parameter set                  */
/***************************************************/
uint8_t GetActiveParamSet(void)
{
	uint8_t setnumber;
	setnumber = eeprom_read_byte(&EEPromArray[PID_ACTIVE_SET]);
	if(setnumber > 5)
	{
		setnumber = 3;
		eeprom_write_byte(&EEPromArray[PID_ACTIVE_SET], setnumber);
	}
	return(setnumber);
}

/***************************************************/
/*       Set active parameter set                  */
/***************************************************/
void SetActiveParamSet(uint8_t setnumber)
{
	if(setnumber > 5) setnumber = 5;
	if(setnumber < 1) setnumber = 1;
	eeprom_write_byte(&EEPromArray[PID_ACTIVE_SET], setnumber);
}

/***************************************************/
/*       Initialize EEPROM Parameter Sets          */
/***************************************************/
void ParamSet_Init(void)
{
	uint8_t Channel_Backup  = 0, i;
	// parameter version  check
	if(eeprom_read_byte(&EEPromArray[PID_PARAM_REVISION]) != EEPARAM_REVISION)
	{
		// if version check faild
		printf("\n\rInit Parameter in EEPROM");
		eeprom_write_byte(&EEPromArray[EEPROM_ADR_MIXER_TABLE], 0xFF); // reset also mixer table
		// check if channel mapping backup is valid
		if(		(eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+0]) < 12)
		    &&  (eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+1]) < 12)
		    &&  (eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+2]) < 12)
		    &&  (eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+3]) < 12)
		   )
		{
			Channel_Backup = 1;
		}
		// fill all 5 parameter settings
		for (i = 1;i < 6; i++)
		{
			switch(i)
			{
				case 1:
					ParamSet_DefaultSet1(); // Fill ParamSet Structure to default parameter set 1 (Sport)
					break;
				case 2:
					ParamSet_DefaultSet2(); // Kamera
					break;
				case 3:
					ParamSet_DefaultSet3(); // Beginner
					break;
				default:
					ParamSet_DefaultSet2(); // Kamera
					break;
			}
			if(Channel_Backup) // if we have a rc channel mapping backup in eeprom
			{
				// restore it
				ParamSet.ChannelAssignment[0] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+0]);
				ParamSet.ChannelAssignment[1] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+1]);
				ParamSet.ChannelAssignment[2] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+2]);
				ParamSet.ChannelAssignment[3] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+3]);
				ParamSet.ChannelAssignment[4] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+4]);
				ParamSet.ChannelAssignment[5] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+5]);
				ParamSet.ChannelAssignment[6] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+6]);
				ParamSet.ChannelAssignment[7] = eeprom_read_byte(&EEPromArray[EEPROM_ADR_CHANNELS+7]);
			}
			ParamSet_WriteToEEProm(i);
		}
		// default-Setting is parameter set 3
		SetActiveParamSet(3);
		// update version info
		SetParamByte(PID_PARAM_REVISION, EEPARAM_REVISION);
	}
	// read active parameter set to ParamSet stucture
	ParamSet_ReadFromEEProm(GetActiveParamSet());
	printf("\n\rUsing Parameter Set %d", GetActiveParamSet());

	// load mixer table
	if(!MixerTable_ReadFromEEProm() )
	{
		printf("\n\rGenerating default Mixer Table");
		MixerTable_Default(); // Quadro
		MixerTable_WriteToEEProm();
	}
	// determine motornumber
	RequiredMotors = 0;
	for(i = 0; i < 16; i++)
	{
		if(Mixer.Motor[i][MIX_GAS] > 0) RequiredMotors++;
	}

	printf("\n\rMixer-Config: '%s' (%u Motors)",Mixer.Name, RequiredMotors);
	printf("\n\r==============================");
}
