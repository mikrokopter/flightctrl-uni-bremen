#ifndef _SPECTRUM_H
#define _SPECTRUM_H

#include <inttypes.h>

#define USART1_BAUD 115200

extern uint8_t SpektrumTimer;
// this function should be called within the UART RX ISR
extern void spectrum_parser(uint8_t c);
#endif //_SPECTRUM_H
