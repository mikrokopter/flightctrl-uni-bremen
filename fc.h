/*!
	\file
	\brief Contains the Flight-Control's main code.
*/

#ifndef _FC_H
#define _FC_H

#include <inttypes.h>



/*!
	\brief Specifies the scaling factor for \ref AccNick and \ref AccRoll to have a higher resolution.

	Example: \code AccNick = ACC_AMPLIFY * AdAccNick \endcode
*/
#define ACC_AMPLIFY  6

/*!
	\brief Specifies the factor used for converting \ref AccNick and \ref AccRoll to their approximate values in degrees.

	Example: \code NickAngleDeg = AccNick / ACC_DEG_FACTOR \endcode

	The value is derived from the datasheet of the acceleration sensor where 5g are scaled to \f$ V_{Ref} \f$ (which is converted to a 10 Bit value or 1024 counts).
	Therefore 1g is equal to \f$ 1024/5 = 205 \f$ counts.
	The \link ISR(ADC_vect) ADC ISR \endlink combines 2 accelerator samples to AdValueAcc
	and 1g yields to \f$ AdValueAcc = 2 * 205 = 410 \f$ which is again scaled by \ref ACC_AMPLIFY
	resulting in 1g being equal to \f$ Acc = 2 * 205 * 6 = 2460 \f$.
	The linear approx of the \f$ arcsin \f$ and the scaling of Acc gives \f$ sin(20^\circ) * 2460 = 841 \f$ and \f$ 841 / 20 = 42 \f$.
*/
#define ACC_DEG_FACTOR  42

// scaling from IntegralGyroNick, IntegralGyroRoll, IntegralGyroYaw -> Attitude in deg
// i.e. Nick Angle in deg = IntegralGyroNick / GYRO_DEG_FACTOR
/*!
	\brief Specifies the factor for converting IntegralGyroNick, IntegralGyroRoll and IntegralGyroYaw to their approximate values in degrees.

	Example: \code NickAngleDeg = IntegralGyroNick / GYRO_DEG_FACTOR \endcode

	\sa ACC_DEG_FACTOR
*/
#define GYRO_DEG_FACTOR  ((int16_t)(ParamSet.GyroAccFactor) * ACC_DEG_FACTOR)

// shift for zero centered rc channel data to Poty and thrust values
#define RC_POTI_OFFSET 110
#define RC_GAS_OFFSET 120

//! Increases the resolution of setpoints.
#define STICK_GAIN 4

#define LIMIT_MIN(value, min) {if(value < min) value = min;}
#define LIMIT_MAX(value, max) {if(value > max) value = max;}

/*!
	\brief Ensures that a given value is between the given bounds and limits it to those bounds when they are exceeded.

	\param value The value to be checked.
	\param min The lower bound.
	\param max The upper bound.
*/
#define LIMIT_MIN_MAX(value, min, max) \
{ \
	if(value < min) { \
		value = min; \
	} else if(value > max) { \
		value = max; \
	} \
}



extern uint8_t RequiredMotors;

typedef struct
{
	uint8_t HeightD;
	uint8_t MaxHeight;
	uint8_t HeightP;
	uint8_t Height_ACC_Effect;
	uint8_t Height_GPS_Z;
	uint8_t CompassYawEffect;
	uint8_t GyroD;
	uint8_t GyroP;
	uint8_t GyroI;
	uint8_t GyroYawP;
	uint8_t GyroYawI;
	uint8_t StickYawP;
	uint8_t IFactor;
	uint8_t UserParam1;
	uint8_t UserParam2;
	uint8_t UserParam3;
	uint8_t UserParam4;
	uint8_t UserParam5;
	uint8_t UserParam6;
	uint8_t UserParam7;
	uint8_t UserParam8;
	uint8_t ServoNickControl;
	uint8_t ServoRollControl;
	uint8_t LoopGasLimit;
	uint8_t AxisCoupling1;
	uint8_t AxisCoupling2;
	uint8_t AxisCouplingYawCorrection;
	uint8_t DynamicStability;
	uint8_t ExternalControl;
	uint8_t J16Timing;
	uint8_t J17Timing;
	#if (defined (USE_KILLAGREG) || defined (USE_MK3MAG))
	uint8_t NaviGpsModeControl;
	uint8_t NaviGpsGain;
	uint8_t NaviGpsP;
	uint8_t NaviGpsI;
	uint8_t NaviGpsD;
	uint8_t NaviGpsACC;
	uint8_t NaviOperatingRadius;
	uint8_t NaviWindCorrection;
	uint8_t NaviSpeedCompensation;
	#endif
	int8_t KalmanK;
	int8_t KalmanMaxDrift;
	int8_t KalmanMaxFusion;
} fc_param_t;

extern fc_param_t FCParam;


// rotation rates
extern  int16_t GyroNick, GyroRoll, GyroYaw;

// attitude calcualted by temporal integral of gyro rates
extern  int32_t IntegralGyroNick, IntegralGyroRoll, IntegralGyroYaw;


// bias values
extern int16_t BiasHiResGyroNick, BiasHiResGyroRoll, AdBiasGyroYaw;
extern int16_t AdBiasAccNick, AdBiasAccRoll;
extern volatile float AdBiasAccTop;

extern volatile int32_t ReadingIntegralTop; // calculated in analog.c

// compass navigation
extern int16_t CompassHeading;
extern int16_t CompassCourse;
extern int16_t CompassOffCourse;
extern uint8_t CompassCalState;
extern int32_t YawGyroHeading;
extern int16_t YawGyroHeadingInDeg;

/*!
	\brief Holds the set height to be held by the height control.
	\todo why are there \code SetPointHeight = ... - 20; \endcode when setting this
*/
extern int32_t SetPointHeight;

// accelerations
extern  int16_t AccNick, AccRoll, AccTop;

/*!
	Pitch acceleration to be sent to navi board.
	It is the accumulation of multiple samples.
	This is reset after the package to be sent to the navi board is assembled.
	\sa NaviCntAcc
*/
extern int16_t NaviAccNick;
/*!
	Roll acceleration to be sent to navi board.
	It is the accumulation of multiple samples.
	This is reset after the package to be sent to the navi board is assembled.
	\sa NaviCntAcc
*/
extern int16_t NaviAccRoll;
/*!
	Holds account of how many samples have been accumulated onto NaviAccPitch and NaviAccRoll.
	This is reset after the package to be sent to the navi board is assembled.
*/
extern int16_t NaviCntAcc;


// looping params
extern long TurnOver180Nick, TurnOver180Roll;

// external control
extern int16_t ExternStickNick, ExternStickRoll, ExternStickYaw;

#define ACC_CALIB 1
#define NO_ACC_CALIB 0

void Motor_Control(void);

/*!
	Establisch neutral readings.
	\param AccAdjustment Whether to recalculate the accelerator biases.
	\sa SetNeutral_RestoreAccBias(), SetNeutral_SaveAccBias()
*/
void SetNeutral(uint8_t AccAdjustment);


extern int16_t  Poti1, Poti2, Poti3, Poti4, Poti5, Poti6, Poti7, Poti8;

// current stick values
extern int16_t StickNick;
extern int16_t StickRoll;
extern int16_t StickYaw;
// current GPS-stick values
extern int16_t GPSStickNick;
extern int16_t GPSStickRoll;

// current stick elongations
extern int16_t MaxStickNick, MaxStickRoll, MaxStickYaw;


extern uint16_t ModelIsFlying;


// MKFlags
#define MKFLAG_MOTOR_RUN  0x01 //!< MK flag set when motors are running
#define MKFLAG_FLY        0x02 //!< MK flag set when the copter is believed to be flying
#define MKFLAG_CALIBRATE  0x04
#define MKFLAG_START      0x08
#define MKFLAG_EMERGENCY_LANDING  0x10 //!< MK flag set when the copter is performing an emergency landing
#define MKFLAG_LOWBAT     0x20
#define MKFLAG_RESERVE2   0x40
#define MKFLAG_RESERVE3   0x80

/*!
	Informs about the current MK state.
*/
extern volatile uint8_t MKFlags;

#endif //_FC_H

