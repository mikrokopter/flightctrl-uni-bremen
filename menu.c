// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) Holger Buss, Ingo Busker
// + Nur für den privaten Gebrauch
// + www.MikroKopter.com
// + porting the sources to other systems or using the software on other systems (except hardware from www.mikrokopter.de) is not allowed
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten (nicht-kommerziellen) Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder sonstigen Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include <stdlib.h>
#include <inttypes.h>
#include "main.h"
#include "hwinfo.h"
#include "eeprom.h"
#include "timer2.h"
#include "fc.h"
#include "rc.h"
#include "uart0.h"
#include "printf_P.h"
#include "analog.h"
#include "twimaster.h"
#include "motor.h"

#ifdef USE_KILLAGREG
#include "mm3.h"
#endif

#if (defined (USE_KILLAGREG) || defined (USE_MK3MAG))
#include "ubx.h"
#endif

#if (!defined (USE_KILLAGREG) && !defined (USE_MK3MAG))
uint8_t MaxMenuItem = 14;
#else
#ifdef USE_MK3MAG
uint8_t MaxMenuItem = 15;
#endif

#ifdef USE_KILLAGREG
uint8_t MaxMenuItem = 17;
#endif
#endif
uint8_t MenuItem = 0;
uint8_t RemoteKeys = 0;

#define KEY1    0x01
#define KEY2    0x02
#define KEY3    0x04
#define KEY4    0x08
#define KEY5    0x10



#define DISPLAYBUFFSIZE 80
int8_t DisplayBuff[DISPLAYBUFFSIZE] = "Hello World";
uint8_t DispPtr = 0;


/************************************/
/*        Clear LCD Buffer          */
/************************************/
void LCD_Clear(void)
{
 uint8_t i;
 for( i = 0; i < DISPLAYBUFFSIZE; i++) DisplayBuff[i] = ' ';
}


/************************************/
/*        Update Menu on LCD        */
/************************************/
// Display with 20 characters in 4 lines
void LCD_PrintMenu(void)
{
	if(RemoteKeys & KEY1)
	{
		if(MenuItem) MenuItem--;
		else MenuItem = MaxMenuItem;
	}
	if(RemoteKeys  & KEY2)
	{
		if(MenuItem == MaxMenuItem) MenuItem = 0;
		else MenuItem++;
	}
	if((RemoteKeys  & KEY1) && (RemoteKeys  & KEY2)) MenuItem = 0;




	LCD_Clear();

	if(MenuItem > MaxMenuItem) MenuItem = MaxMenuItem;
	// print menu item number in the upper right corner
	if(MenuItem < 10)
	{
		LCD_printfxy(17,0,"[%i]",MenuItem);
	}
	else
	{
		LCD_printfxy(16,0,"[%i]",MenuItem);
	}

	switch(MenuItem)
	{
	case 0:// Version Info Menu Item
	    LCD_printfxy(0,0,"++ Flight-Ctrl ++");
		LCD_printfxy(0,1,"HW:V%d.%d SW:%d.%d%c",HWInfo_BoardRevision_Major(),HWInfo_BoardRevision_Minor(),VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH+'a');
		LCD_printfxy(0,2,"Setting: %d %s", GetActiveParamSet(), Mixer.Name);
		if(TWI_Timeout < 6)
		{
			LCD_printfxy(0,3,"I2C Error!!!");
		}
		else if (MissingMotor)
		{
			LCD_printfxy(0,3,"Missing BL-Ctrl:%d", MissingMotor);
		}
		else LCD_printfxy(0,3,"(c) Holger Buss");
		break;
	case 1:// Height Control Menu Item
		if(ParamSet.Config0 & CFG0_AIRPRESS_SENSOR)
		{
			LCD_printfxy(0,0,"Height:    %5i",(int16_t)(ReadingHeight/5));
			LCD_printfxy(0,1,"Set Point: %5i",(int16_t)(SetPointHeight/5));
			LCD_printfxy(0,2,"Air Press.:%5i",AdAirPressure);
			LCD_printfxy(0,3,"Offset    :%5i",OCR0A);
		}
		else
		{
			LCD_printfxy(0,1,"No ");
			LCD_printfxy(0,2,"Height Control");
		}
		break;
	case 2:// Attitude Menu Item
		LCD_printfxy(0,0,"Attitude");
		LCD_printfxy(0,1,"Nick:      %5i",IntegralGyroNick/1024);
		LCD_printfxy(0,2,"Roll:      %5i",IntegralGyroRoll/1024);
		LCD_printfxy(0,3,"Heading:   %5i",CompassHeading);
		break;
	case 3:// Remote Control Channel Menu Item
		LCD_printfxy(0,0,"C1:%4i  C2:%4i ",PPM_in[1],PPM_in[2]);
		LCD_printfxy(0,1,"C3:%4i  C4:%4i ",PPM_in[3],PPM_in[4]);
		LCD_printfxy(0,2,"C5:%4i  C6:%4i ",PPM_in[5],PPM_in[6]);
		LCD_printfxy(0,3,"C7:%4i  C8:%4i ",PPM_in[7],PPM_in[8]);
		break;
	case 4:// Remote Control Mapping Menu Item
		LCD_printfxy(0,0,"Ni:%4i  Ro:%4i ",PPM_in[ParamSet.ChannelAssignment[CH_NICK]],PPM_in[ParamSet.ChannelAssignment[CH_ROLL]]);
		LCD_printfxy(0,1,"Gs:%4i  Ya:%4i ",PPM_in[ParamSet.ChannelAssignment[CH_GAS]] + RC_GAS_OFFSET,PPM_in[ParamSet.ChannelAssignment[CH_YAW]]);
		LCD_printfxy(0,2,"P1:%4i  P2:%4i ",PPM_in[ParamSet.ChannelAssignment[CH_POTI1]] + RC_GAS_OFFSET, PPM_in[ParamSet.ChannelAssignment[CH_POTI2]] + RC_POTI_OFFSET);
		LCD_printfxy(0,3,"P3:%4i  P4:%4i ",PPM_in[ParamSet.ChannelAssignment[CH_POTI3]] + RC_POTI_OFFSET, PPM_in[ParamSet.ChannelAssignment[CH_POTI4]] + RC_POTI_OFFSET);
		break;
	case 5:// Gyro Sensor Menu Item
		LCD_printfxy(0,0,"Gyro - Sensor");
		switch(HWInfo_BoardRevision())
		{
		case Revision_10:
			LCD_printfxy(0,1,"Nick %4i (%3i.%i)",AdValueGyroNick - BiasHiResGyroNick / HIRES_GYRO_AMPLIFY, BiasHiResGyroNick / HIRES_GYRO_AMPLIFY, BiasHiResGyroNick % HIRES_GYRO_AMPLIFY);
			LCD_printfxy(0,2,"Roll %4i (%3i.%i)",AdValueGyroRoll - BiasHiResGyroRoll / HIRES_GYRO_AMPLIFY, BiasHiResGyroRoll / HIRES_GYRO_AMPLIFY, BiasHiResGyroRoll % HIRES_GYRO_AMPLIFY);
			LCD_printfxy(0,3,"Yaw  %4i (%3i)",AdBiasGyroYaw  - AdValueGyroYaw , AdBiasGyroYaw);
			break;

		case Revision_11:
		case Revision_12:
		case Revision_20: // divice Offests by 2 becuse 2 samples are added in adc isr
			LCD_printfxy(0,1,"Nick %4i (%3i.%i)",AdValueGyroNick - BiasHiResGyroNick/HIRES_GYRO_AMPLIFY, BiasHiResGyroNick / (HIRES_GYRO_AMPLIFY * 2), (BiasHiResGyroNick % (HIRES_GYRO_AMPLIFY * 2)) / 2); // division by 2 to push the reminder below 10 (15/2 = 7)
			LCD_printfxy(0,2,"Roll %4i (%3i.%i)",AdValueGyroRoll - BiasHiResGyroRoll/HIRES_GYRO_AMPLIFY, BiasHiResGyroRoll / (HIRES_GYRO_AMPLIFY * 2), (BiasHiResGyroRoll % (HIRES_GYRO_AMPLIFY * 2)) / 2); // division by 2 to push the reminder below 10 (15/2 = 7)
			LCD_printfxy(0,3,"Yaw  %4i (%3i)",AdBiasGyroYaw  - AdValueGyroYaw , AdBiasGyroYaw/2);
			break;

		case Revision_13:
		default: // divice Offests by 2 becuse 2 samples are added in adc isr
			LCD_printfxy(0,1,"Nick %4i (%3i.%i)(%3i)",AdValueGyroNick - BiasHiResGyroNick/HIRES_GYRO_AMPLIFY, BiasHiResGyroNick / (HIRES_GYRO_AMPLIFY * 2), (BiasHiResGyroNick % (HIRES_GYRO_AMPLIFY * 2))/2, DacOffsetGyroNick); // division by 2 to push the reminder below 10 (15/2 = 7)
			LCD_printfxy(0,2,"Roll %4i (%3i.%i)(%3i)",AdValueGyroRoll - BiasHiResGyroRoll/HIRES_GYRO_AMPLIFY, BiasHiResGyroRoll / (HIRES_GYRO_AMPLIFY * 2), (BiasHiResGyroRoll % (HIRES_GYRO_AMPLIFY * 2))/2, DacOffsetGyroRoll); // division by 2 to push the reminder below 10 (15/2 = 7)
			LCD_printfxy(0,3,"Yaw  %4i (%3i)(%3i)",AdBiasGyroYaw  - AdValueGyroYaw , AdBiasGyroYaw/2, DacOffsetGyroYaw );
			break;
		}
		break;
	case 6:// Acceleration Sensor Menu Item
		LCD_printfxy(0,0,"ACC - Sensor");
		LCD_printfxy(0,1,"Nick   %4i (%3i)",AdValueAccNick/2, AdBiasAccNick/2); // factor 2 because of adding 2 samples in ADC ISR
		LCD_printfxy(0,2,"Roll   %4i (%3i)",AdValueAccRoll/2, AdBiasAccRoll/2); // factor 2 because of adding 2 samples in ADC ISR
		LCD_printfxy(0,3,"Height %4i (%3i)",AdValueAccTop, (int16_t)AdBiasAccTop);
		break;
	case 7:// Accumulator Voltage / Remote Control Level
		LCD_printfxy(0,0,"Voltage:   %3i.%1iV",UBat/10, UBat%10);
		LCD_printfxy(0,1,"RC-Level:   %4i", RC_Quality);
		LCD_printfxy(0,2,"RC-Channels:%4i", RC_Channels);
		LCD_printfxy(0,3,"RC-RSSI:    %4i", RC_RSSI);
		break;
	case 8:// Compass Menu Item
		LCD_printfxy(0,0,"Compass       ");
		LCD_printfxy(0,1,"Course:    %5i",CompassCourse);
		LCD_printfxy(0,2,"Heading:   %5i",CompassHeading);
		LCD_printfxy(0,3,"OffCourse: %5i",CompassOffCourse);
		break;
	case 9:// Poti Menu Item
		LCD_printfxy(0,0,"Po1: %3i Po5: %3i" ,Poti1,Poti5); //PPM24-Extesion
		LCD_printfxy(0,1,"Po2: %3i Po6: %3i" ,Poti2,Poti6); //PPM24-Extesion
		LCD_printfxy(0,2,"Po3: %3i Po7: %3i" ,Poti3,Poti7); //PPM24-Extesion
		LCD_printfxy(0,3,"Po4: %3i Po8: %3i" ,Poti4,Poti8); //PPM24-Extesion
		break;
	case 10:// Servo Menu Item
		LCD_printfxy(0,0,"Servo  " );
		LCD_printfxy(0,1,"Setpoint  %3i",FCParam.ServoNickControl);
		LCD_printfxy(0,2,"Position: %3i",ServoNickValue);
		LCD_printfxy(0,3,"Range:%3i-%3i",ParamSet.ServoNickMin, ParamSet.ServoNickMax);
		break;
	case 11://Extern Control
		LCD_printfxy(0,0,"ExternControl  " );
		LCD_printfxy(0,1,"Ni:%4i  Ro:%4i ",ExternControl.Nick, ExternControl.Roll);
		LCD_printfxy(0,2,"Gs:%4i  Ya:%4i ",ExternControl.Gas, ExternControl.Yaw);
		LCD_printfxy(0,3,"Hi:%4i  Cf:%4i ",ExternControl.Height, ExternControl.Config);
		break;

	case 12://BL Communication errors
		LCD_printfxy(0,0,"BL-Ctrl Errors " );
		LCD_printfxy(0,1," %3d  %3d  %3d  %3d ",Motor[0].Error,Motor[1].Error,Motor[2].Error,Motor[3].Error);
		LCD_printfxy(0,2," %3d  %3d  %3d  %3d ",Motor[4].Error,Motor[5].Error,Motor[6].Error,Motor[7].Error);
		LCD_printfxy(0,3," %3d  %3d  %3d  %3d ",Motor[8].Error,Motor[9].Error,Motor[10].Error,Motor[11].Error);
		break;

	case 13://BL Overview
		LCD_printfxy(0,0,"BL-Ctrl found " );
		LCD_printfxy(0,1," %c   %c   %c   %c ",Motor[0].Present + '-',Motor[1].Present + '-',Motor[2].Present + '-',Motor[3].Present + '-');
		LCD_printfxy(0,2," %c   %c   %c   %c ",Motor[4].Present + '-',Motor[5].Present + '-',Motor[6].Present + '-',Motor[7].Present + '-');
		LCD_printfxy(0,3," %c   -   -   - ",Motor[8].Present + '-');
		if(Motor[9].Present)  LCD_printfxy(4,3,"10");
		if(Motor[10].Present) LCD_printfxy(8,3,"11");
		if(Motor[11].Present) LCD_printfxy(12,3,"12");
		break;
	case 14:// flight time counter
		LCD_printfxy(0,0,"Flight-Time  " );
		LCD_printfxy(0,1,"Trip: %5u min",FlightMinutes);
		LCD_printfxy(0,2,"Total:%5u min",FlightMinutesTotal);
		LCD_printfxy(13,3,"(reset)");
		if(RemoteKeys & KEY4)
		{
			FlightMinutes = 0;
			SetParamWord(PID_FLIGHT_MINUTES, FlightMinutes);
		}
		break;

	#if (defined (USE_KILLAGREG) || defined (USE_MK3MAG))
	case 15://GPS Lat/Lon coords
			if (GPSInfo.status == INVALID)
			{
				LCD_printfxy(0,0,"No GPS data!");
			}
			else
			{
				switch (GPSInfo.satfix)
				{
				case SATFIX_NONE:
					LCD_printfxy(0,0,"Sats: %d Fix: No", GPSInfo.satnum);
					break;
				case SATFIX_2D:
					LCD_printfxy(0,0,"Sats: %d Fix: 2D", GPSInfo.satnum);
					break;
				case SATFIX_3D:
					LCD_printfxy(0,0,"Sats: %d Fix: 3D", GPSInfo.satnum);
					break;
				default:
					LCD_printfxy(0,0,"Sats: %d Fix: ??", GPSInfo.satnum);
					break;
				}
				int16_t i1,i2,i3;
				i1 = (int16_t)(GPSInfo.longitude/10000000L);
				i2 = abs((int16_t)((GPSInfo.longitude%10000000L)/10000L));
				i3 = abs((int16_t)(((GPSInfo.longitude%10000000L)%10000L)/10L));
				LCD_printfxy(0,1,"Lon: %d.%03d%03d deg",i1, i2, i3);
				i1 = (int16_t)(GPSInfo.latitude/10000000L);
				i2 = abs((int16_t)((GPSInfo.latitude%10000000L)/10000L));
				i3 = abs((int16_t)(((GPSInfo.latitude%10000000L)%10000L)/10L));
				LCD_printfxy(0,2,"Lat: %d.%03d%03d deg",i1, i2, i3);
				i1 = (int16_t)(GPSInfo.altitude/1000L);
				i2 = abs((int16_t)(GPSInfo.altitude%1000L));
				LCD_printfxy(0,3,"Alt: %d.%03d m",i1, i2);
			}
			break;
	#endif
	#ifdef USE_KILLAGREG
	case 16:// MM3 Kompass
			LCD_printfxy(0,0,"MM3 Offset");
			LCD_printfxy(0,1,"X_Offset:  %3i",MM3_calib.X_off);
			LCD_printfxy(0,2,"Y_Offset:  %3i",MM3_calib.Y_off);
			LCD_printfxy(0,3,"Z_Offset:  %3i",MM3_calib.Z_off);
			break;
	case 17://MM3 Range
			LCD_printfxy(0,0,"MM3 Range");
			LCD_printfxy(0,1,"X_Range:  %4i",MM3_calib.X_range);
			LCD_printfxy(0,2,"Y_Range:  %4i",MM3_calib.Y_range);
			LCD_printfxy(0,3,"Z_Range:  %4i",MM3_calib.Z_range);
			break;
	#endif

	default:
		MaxMenuItem = MenuItem - 1;
		MenuItem = 0;
		break;
	}
	RemoteKeys = 0;
}
