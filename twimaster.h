/*!
	\file
	\brief Contains the TWI state machine and methods an variables for using the TWI/I2C bus.

	It serves two purposes:
	- update the motor setpoints by talking to the BLs
	- update the gyro offsets by talking to the DAC
*/

#ifndef _TWI_MASTER_H
#define _TWI_MASTER_H

#include <inttypes.h>



//! TWI clock in Hz.
#define SCL_CLOCK  200000L

#define TWI_STATE_MOTOR_TX  0
#define TWI_STATE_MOTOR_RX  3
#define TWI_STATE_GYRO_OFFSET_TX  7



#if defined(__DOXYGEN__)
	/*!
		Interrupt service routine (ISR) for the TWI bus.

		\todo explain TWI ISR in detail (for now see ISR code)
	*/
	#define ISR(TWI_vect)
#endif



extern uint8_t MissingMotor;
extern volatile uint8_t TWI_MotorReadIndex;
extern volatile uint16_t TWI_Timeout;



/*!
	Initialize the TWI.
	\sa TWI_Start()
*/
void TWI_Init(void);

/*!
	Reads the content of the TWI Data Register (TWDR).
	\return The content of TWDR.
	\warning Only use this in the \link ISR(TWI_vect) TWI ISR \endlink .
*/
uint8_t TWI_ReadByte(void);

/*!
	Tell the sender that we want to receive one byte.
	The data can be read with TWI_ReadByte() in the next invokation of the \link ISR(TWI_vect) TWI ISR \endlink .
	\note will send ACK
	\warning Only use this in the \link ISR(TWI_vect) TWI ISR \endlink .
	\sa TWI_ReceiveLastByte(), TWI_ReadByte()
*/
void TWI_ReceiveByte(void);

/*!
	Tell the sender that we want to receive one last byte.
	The data can be read with TWI_ReadByte() in the next invokation of the \link ISR(TWI_vect) TWI ISR \endlink .
	\note will send NACK
	\warning Only use this in the \link ISR(TWI_vect) TWI ISR \endlink .
	\sa TWI_ReceiveByte(), TWI_ReadByte()
*/
void TWI_ReceiveLastByte(void);

/*!
	Reset the TWI.
*/
void TWI_Reset(void);

/*!
	Start the TWI.
	\sa TWI_Stop(), TWI_StopAndSet()
*/
void TWI_Start(uint8_t startState);

/*!
	Retrieve the current state of the TWI state machine.
	\return The current state.
	\sa TWI_State
*/
uint8_t TWI_State_Get(void);

/*!
	Sets the current state of the TWI state machine to the given state.
	This is acted upon on the next invocation of the \link ISR(TWI_vect) TWI ISR \endlink .
	\param newState The new state.
	\sa TWI_State
*/
void TWI_State_Set(uint8_t newState);

/*!
	Stops the TWI.
	\sa TWI_Start(), TWI_StopAndSet()
*/
void TWI_Stop(void);

/*!
	Stop the TWI and leave the state machine in \p state.
	\param state The state to leave the state machine in.
	\sa TWI_Start(), TWI_State_Set(). TWI_StopTWI_Stop()
*/
void TWI_StopAndSet(uint8_t state);

/*!
	Write one byte to the bus.
	\warning Only use this in the \link ISR(TWI_vect) TWI ISR \endlink .
*/
void TWI_WriteByte(int8_t byte);

#endif
