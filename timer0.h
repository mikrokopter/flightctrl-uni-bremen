/*!
	\file
	\brief Contains functions to manage timers and timouts.
*/

#ifndef _TIMER0_H
#define _TIMER0_H

#include <inttypes.h>
#include <stdbool.h>



/*!
	\brief Represents a timeout timer.

	This is a convenience definition to make the timeout API more obvious.
	It should be the same type as \ref CountMilliseconds.
	\sa Timer_TimeoutIn, Timer_HasTimedOut
*/
typedef uint16_t timeout_t;



extern volatile uint16_t cntKompass;

/*!
	\brief Holds account of the passed time at a millisecond resolution.

	This is set in the ISR for Timer0.
*/
extern volatile uint16_t CountMilliseconds;

/*!
	Defines whether new data should be sent to the motors.
	This is set to \c true every 2ms in the ISR of Timer0 and set reset to \c false in the main loop.
	\warning Care must be taken to allow serial data transmission only when this is \c false.
	\sa ISR(TIMER0_OVF_vect), Main_Loop
*/
extern volatile bool UpdateMotor;

#ifdef USE_NAVICTRL
	extern volatile uint8_t SendSPI;
#endif



/*!
	\brief Initializes Timer0.

	Timer0 is used for the PWM generation to control the offset voltage at the air pressure sensor.
	Its overflow interrupt routine is used to generate the beep signal and the flight control motor update rate.

	\todo Clean out \c (0<<FOO)
*/
extern void Timer_Timer0_Init(void);

/*!
	\brief Checks whether a given timeout timer has timed out.
	\param timeout A timeout timer.
	\return \c true if the timeout timer has timed out, \c false otherwise.
	\sa Timer_TimeoutIn
*/
extern bool Timer_HasTimedOut(timeout_t timeout);

/*!
	\brief Creates a timeout timer.

	This timer times out after \a ms milliseconds which can be checked with Timer_HasTimedOut().
	\param ms The time out in milliseconds.
	\return A timeout timer.
	\sa Timer_HasTimedOut
*/
extern timeout_t Timer_TimeoutIn(uint16_t ms);

/*!
	\brief Waits for a given amout of time.
	\param ms The time to wait in milliseconds.
	\warning This function blocks for the specified time.
	\sa Timer_WaitWithMeasurementFor()
*/
extern void Timer_WaitFor(uint16_t ms);

/*!
	\brief Waits for a given amout of time.

	\param ms The time to wait in milliseconds.
	\note In contrast to Timer_WaitFor() it still allows for the ADC to work.
	\warning This function blocks for the specified time.
	\sa Timer_WaitFor()
*/
extern void Timer_WaitWithMeasurementFor(uint16_t ms);



#if defined(__DOXYGEN__)
/*!
	ISR (interrupt service routine) of Timer0.

	- It sends a byte to the Navi-Control via the ISP-Bus if appropriate.
	- It sets \ref UpdateMotor to 1 every 20th run.
	- It updates the \ref CountMilliseconds variable.
	- It controls the beeping of the speaker.
	- It update compass value if enabled in the settings
	\note This is called with a frequency of 9.765 kHz.(~ every 0.1ms)
*/
#define ISR(TIMER0_OVF_vect)
#endif



#endif // _TIMER0_H
