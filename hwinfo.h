/*!
	\file
	\brief
*/

#ifndef _BOARD_H
#define _BOARD_H

#include <stdbool.h>
#include <stdint.h>



#if !defined(F_CPU) || defined(__DOXYGEN__)
	// not set in the Makefile
	//! The controller's system clock/CPU frequency in Hz.
	#define F_CPU  20000000
#endif

/*!
	The controller's system clock/CPU frequency in Hz.

	\note This define is deprecated and should be abandoned in favour of #F_CPU.
	\sa F_CPU
*/
#define SYSCLK  F_CPU



/*!
	The list of supported hardware revisions.

	\sa HWInfo_DetermineBoardRevision(), HWInfo_BoardRevision()
*/
typedef enum {
	Revision_Unknown = 0,
	Revision_10 = 10,
	Revision_11 = 11,
	Revision_12 = 12,
	Revision_13 = 13,
	Revision_20 = 20
} BoardRevision_t;

//! The list of supported CPUs.
typedef enum {
	CPU_Unknown = 0,
	ATMEGA644,
	ATMEGA644P
} CPUType_t;



BoardRevision_t HWInfo_BoardRevision(void);

/*!
	Gives you the major verion number of the FlightCtrl board revision.

	\return The major revision number (e.g. 1 for 1.3).
*/
uint8_t HWInfo_BoardRevision_Major(void);

/*!
	Gives you the minor verion number of the FlightCtrl board revision.

	\return The minor revision number (e.g. 3 for 1.3).
*/
uint8_t HWInfo_BoardRevision_Minor(void);

CPUType_t HWInfo_CPUType(void);

/*!
	Determines the revision of the FlightCtrl board.

	Have a look at #BoardRevision_t for a list of supported revisions.
*/
void HWInfo_DetermineBoardRevision(void);

/*!
	Determines the type of the CPU used.

	Have a look at #CPUType_t for a list of supported revisions.

	\warning This works only after reset or power on when the registers have default values.
*/
void HWInfo_DetermineCPUType(void);



#endif // _BOARD_H
