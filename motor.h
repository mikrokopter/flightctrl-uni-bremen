/*!
	\file
	Contains functions, structures and variables for controlling the motors.
*/

#ifndef _MOTOR_H
#define _MOTOR_H

#include<inttypes.h>



/*! Struct for storing the mixer table in.
	\warning For data compatibility reasons #EEMIXER_REVISION should be incremented, when this gets changed.
*/
typedef struct
{
	uint8_t Revision;
	int8_t Name[12];
	int8_t Motor[16][4];
}
#if !defined(__DOXYGEN__)
__attribute__((packed))
#endif
MixerTable_t;

typedef struct
{
	/*!
		The current motor set point.
		This value is sent to BL in the \link ISR(TWI_vect) TWI ISR\endlink.
		\sa Motor_Mixer()
	*/
	uint8_t Setpoint;
	uint8_t Present; //!< 0 if BL was found
	uint8_t Error;   //!< I2C error counter
	uint8_t Current; //!< read back from BL
	uint8_t MaxPWM;  //!< read back from BL
}
#if !defined(__DOXYGEN__)
__attribute__((packed))
#endif
MotorData_t;



/*!
	Defines the maximum number of motors on the MK.
	This can be greater than the actual number of motors.
	\todo specialize this for different types of MKs
*/
#define Motor_Count  12

#define Motor_Index_Front  0 //! \todo check if index is correct
#define Motor_Index_Rear   1 //! \todo check if index is correct
#define Motor_Index_Right  2 //! \todo check if index is correct
#define Motor_Index_Left   3 //! \todo check if index is correct

#define MIX_GAS   0
#define MIX_NICK  1
#define MIX_ROLL  2
#define MIX_YAW   3

//! Maximum mixer value.
#define MIXER_MAX  64L



/*!
	Universal mixer.
	Uses the given mixer fractions and combines them for motor control.
	It \link Motor_SmoothValues() filters spikes \endlink and updates the \link MotorData_t.Setpoint motor setpoints\endlink.
	\param gasMixFraction
	\param pitchMixFraction
	\param rollMixFraction
	\param yawMixFraction
*/
void Motor_Mixer(uint16_t gasMixFraction, uint16_t pitchMixFraction, uint16_t rollMixFraction, uint16_t yawMixFraction);

/*!
	Transmit motor data via TWI.
*/
void Motor_SendData(void);

/*!
	Smoothes motor values to filter spikes.
	\param oldValue The old motor value.
	\param newValue The new motor value.
	\return The smoothed motor value.
*/
int16_t Motor_SmoothValues(int16_t oldValue, int16_t newValue);



//! The current mixer table.
extern MixerTable_t Mixer;

extern MotorData_t Motor[Motor_Count];



#endif // _MOTOR_H
