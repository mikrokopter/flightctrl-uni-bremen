// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) Holger Buss, Ingo Busker
// + Nur für den privaten Gebrauch
// + www.MikroKopter.com
// + porting the sources to other systems or using the software on other systems (except hardware from www.mikrokopter.de) is not allowed
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten (nicht-kommerziellen) Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder sonstigen Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <avr/io.h>
#include <avr/interrupt.h>

#include "hwinfo.h"
#include "uart1.h"



#if defined(USE_KILLAGREG) || defined(USE_MK3MAG)
	#include "ubx.h"
#else
	#ifdef USE_RC_DSL
		#include "dsl.h"
	#endif
	#ifdef USE_RC_SPEKTRUM
		#include "spectrum.h"
	#endif
#endif

#ifndef USART1_BAUD
	#define USART1_BAUD  57600
#endif



/****************************************************************/
/*              Initialization of the USART1                    */
/****************************************************************/
void USART1_Init (void)
{
	// USART1 Control and Status Register A, B, C and baud rate register
	uint8_t sreg = SREG;
	uint16_t ubrr = (uint16_t) ((uint32_t) SYSCLK/(8 * USART1_BAUD) - 1);

	// disable all interrupts before reconfiguration
	cli();

	UCSR1B &= ~(1 << RXCIE1); // disable RX interrupt
	UCSR1B &= ~(1 << TXCIE1); // disable TX interrupt
	UCSR1B &= ~(1 << UDRIE1); // disable DRE interrupt

	// set direction of RXD1 and TXD1 pins
	// set RXD1 (PD2) as an input pin
	PORTD |=  (1 << PORTD2);
	DDRD  &= ~(1 << DDD2);

	// set TXD1 (PD3) as an output pin
	PORTD |= (1 << PORTD3);
	DDRD  |= (1 << DDD3);

	// USART0 Baud Rate Register
	// set clock divider
	UBRR1H = (uint8_t)(ubrr >> 8);
	UBRR1L = (uint8_t)ubrr;

	// enable double speed operation
	UCSR1A |= (1 << U2X1);
	// enable receiver and transmitter
	UCSR1B = (1 << TXEN1) | (1 << RXEN1);
	// set asynchronous mode
	UCSR1C &= ~(1 << UMSEL11);
	UCSR1C &= ~(1 << UMSEL10);
	// no parity
	UCSR1C &= ~(1 << UPM11);
	UCSR1C &= ~(1 << UPM10);
	// 1 stop bit
	UCSR1C &= ~(1 << USBS1);
	// 8-bit
	UCSR1B &= ~(1 << UCSZ12);
	UCSR1C |=  (1 << UCSZ11);
	UCSR1C |=  (1 << UCSZ10);

	// flush receive buffer explicit
	while ( UCSR1A & (1<<RXC1) ) UDR1;

	// enable interrupts at the end
	UCSR1B |= (1 << RXCIE1); // enable RX interrupt
	//UCSR1B |= (1 << TXCIE1); // enable TX interrupt
	//UCSR1B |= (1 << UDRIE1); // enable DRE interrupt

	// restore global interrupt flags
	SREG = sreg;
}



/****************************************************************/
/*               USART1 data register empty ISR                 */
/****************************************************************/
/*ISR(USART1_UDRE_vect)
{
}
*/

/****************************************************************/
/*               USART1 transmitter ISR                         */
/****************************************************************/
/*ISR(USART1_TX_vect)
{
}
*/

/****************************************************************/
/*               USART1 receiver ISR                            */
/****************************************************************/
ISR(USART1_RX_vect)
{
	uint8_t c;
	c = UDR1; // get data byte
	#if (defined(USE_KILLAGREG) || defined(USE_MK3MAG))
		ubx_parser(c); // and put it into the ubx protocol parser
	#else
		#ifdef USE_RC_DSL
			dsl_parser(c); // parse dsl data stream
		#endif
		#ifdef USE_RC_SPEKTRUM
			spectrum_parser(c); // parse spectrum data stream
		#endif
	#endif
}
