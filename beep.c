
#include "beep.h"

#include <avr/io.h>

#include "fc.h"
#include "hwinfo.h"
#include "timer0.h"



volatile uint16_t BeepModulation = 0xFFFF;
volatile uint16_t BeepTime = 0;



void Beep_Beep(uint8_t times, uint16_t duration)
{
	// never with running motors!
	if(MKFlags & MKFLAG_MOTOR_RUN) return;

	while(times--)
	{
		Beep_BeepTime_Set(duration); // in ms second
		Timer_WaitFor(duration * 2); // blocks 2 times beep duration as pause to next beep
	}
}

inline void Beep_BeepTime_Set(uint16_t point10ms)
{
	BeepTime = point10ms;
}

inline bool Beep_IsBeeping(void)
{
	return BeepTime != 0;
}

void Beep_Set(bool on)
{
	if(on)
	{ // set speaker port to high
		if(HWInfo_BoardRevision() == Revision_10) PORTD |= _BV(PORTD2); // speaker at PD2
		else                                      PORTC |= _BV(PORTC7); // speaker at PC7
	}
	else
	{ // set speaker port to low
		if(HWInfo_BoardRevision() == Revision_10) PORTD &= ~_BV(PORTD2); // speaker at PD2
		else                                      PORTC &= ~_BV(PORTC7); // speaker at PC7
	}
}
